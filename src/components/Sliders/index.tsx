import React, { FC } from "react";
import {
  Slider,
  SliderTrack,
  SliderFilledTrack,
  SliderThumb,
  useColorModeValue,
} from "@chakra-ui/react";
import { Tooltip } from "../Tooltip";

interface SliderValueTypes {
  title: string;
  description: string;
  value: number;
}
interface SliderProp {
  sliderValue: SliderValueTypes;
  isDisabled?: boolean;
  min: number;
  max: number;
}

export const Sliders: FC<SliderProp> = ({ sliderValue, isDisabled, min, max }) => {
  const [sliderVal, setSliderVal] = React.useState(sliderValue.value);
  const [showTooltip, setShowTooltip] = React.useState(false);

  return (
    <>
      <Slider
        isDisabled={isDisabled}
        aria-label="slider-ex-1"
        defaultValue={sliderVal}
        min={min | 0}
        max={max}
        onChange={(v) => setSliderVal(v)}
        onMouseEnter={() => setShowTooltip(true)}
        onMouseLeave={() => setShowTooltip(false)}
      >
        <SliderTrack
          bg={useColorModeValue("secondaryStroke.500", "secondaryBackground.200")}
        >
          <SliderFilledTrack _disabled={{bg: useColorModeValue("#8799DA", "#6676B7")}} _hover={{bg: useColorModeValue("secondary.500", "primary.350")}} bg={useColorModeValue("primaryGlow", "primaryIcon.500")} />
        </SliderTrack>
        <Tooltip
          title={sliderValue.title}
          isOpen={showTooltip}
          description={sliderValue.description}
          placement={"bottom"}
        >
          <SliderThumb _dark={{ bg: "frethanWhite" }} />
        </Tooltip>
      </Slider>
    </>
  );
};
