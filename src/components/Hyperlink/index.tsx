import { ArrowLeftIcon, ArrowRightIcon } from '../../icons'
import { hypertextColors, hypertextSizes } from '../../constants'
import {
  HyperlinkTextSize,
  HyperlinkTextVariants,
  HyperlinkVariants,
} from '../../lib/types'
import { Link, HStack } from '@chakra-ui/react'
import React from 'react'
import { useMemo } from 'react'

interface HyperlinkProps {
  text: string
  leftArrow?: boolean
  rightArrow?: boolean
  size: HyperlinkTextSize
  variants?: HyperlinkTextVariants
}

export const Hyperlink = ({
  text,
  leftArrow,
  rightArrow,
  size,
  variants,
}: HyperlinkProps) => {
  const currentVariant = variants || HyperlinkVariants.HYPERLINK_NORMAL
  const isVariantGlow =
    currentVariant && currentVariant === HyperlinkVariants.HYPERLINK_GLOW
  const hypertextColor = useMemo(() => {
    return hypertextColors[currentVariant]
  }, [currentVariant])

  const hyperlinSize = useMemo(() => {
    return hypertextSizes[size]
  }, [size])

  return (
    <HStack
      spacing={2}
      backgroundColor={isVariantGlow ? 'frethanWhite' : 'transparent'}
      border={isVariantGlow ? '5px solid rgba(78, 138, 247, 0.3)' : 'none'}
      borderRadius="8px"
      alignItems="center"
      w="fit-content"
      p={isVariantGlow ? '2px 5px' : '0'}
    >
      {leftArrow && (
        <ArrowLeftIcon
          color={hypertextColor}
          h={hyperlinSize.arrowProp.height}
          w={hyperlinSize.arrowProp.width}
        />
      )}
      <Link
        variant={currentVariant}
        fontSize={hyperlinSize.textProp.fontSize}
        lineHeight={hyperlinSize.textProp.lineHeight}
        color={hypertextColor}
      >
        {text}
      </Link>
      {rightArrow && (
        <ArrowRightIcon
          color={hypertextColor}
          h={hyperlinSize.arrowProp.height}
          w={hyperlinSize.arrowProp.width}
        />
      )}
    </HStack>
  )
}
