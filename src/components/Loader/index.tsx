import React from "react";
import { Spinner, useColorModeValue } from "@chakra-ui/react";
import { LoaderSizes } from "../../lib/types";

interface LoadersPropTypes {
  thickness: string;
  speed: string;
  emptyColor: string;
  color: string;
  size: LoaderSizes;
}

export const Loaders = ({
  thickness,
  speed,
  emptyColor,
  color,
  size,
}: LoadersPropTypes) => {
  return (
    <Spinner
      thickness={thickness}
      speed={speed}
      emptyColor={useColorModeValue("#EBEFFF","#324073")}
      size={size}
      color="goldGradient.200"
    />
  );
};
