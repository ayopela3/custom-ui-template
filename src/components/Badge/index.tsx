import React, { FC, MouseEventHandler, useEffect, useState } from 'react'
import { Button , Icon, Text } from '@chakra-ui/react'
import { COLORSCHEME, SIZE, VARIANTS } from '../../constants/options'
import { CloseIcon } from '../../icons'
import { InputIconProps } from '../../lib/types'

interface BadgeProps {
    leftIcon?: InputIconProps | any
    label: string
    rightIcon?: InputIconProps | any
    backgroundColor?: string 
    colorScheme: COLORSCHEME
    textColor?: string
    size?: SIZE
    variant?: VARIANTS 
    onClick?: MouseEventHandler 
}

export const Badge: FC<BadgeProps> = ({ 
    leftIcon,
    label,
    rightIcon,
    backgroundColor,
    colorScheme,
    textColor,
    size,
    variant,
    onClick,
}) => {
    const [fontSize, setFontSize] = useState(11)
    const [height, setHeight] = useState(20)
    useEffect(() => {
        if (size === 'sm') {
            setFontSize(12)
            setHeight(20)
        } else if (size === 'md') {
            setFontSize(12)
            setHeight(24)
        } else {
            setFontSize(14)
            setHeight(32)
        }
    } , 
    [size]);
    return (
        <Button
            style={{borderRadius: 8, maxHeight: 32, height }}
            leftIcon={(leftIcon.icon === undefined ? 
            leftIcon :
            <Icon
                as={leftIcon.icon}
                color={leftIcon.color || "primary.icon"}
                h={leftIcon.height}
                w={leftIcon.width}
              />)}
            rightIcon={(rightIcon.icon === undefined ? 
            rightIcon :
            <Icon
                as={rightIcon.icon}
                color={rightIcon.color || "primary.icon"}
                h={rightIcon.height}
                w={rightIcon.width}
              />)}
            bg={backgroundColor}
            colorScheme={colorScheme}
            textColor={textColor}
            size={size} 
            onClick={onClick} 
            variant={variant}>
           <Text style={{fontSize}}>{label}</Text>
        </Button>
    )
}

Badge.defaultProps = {
    leftIcon: {icon: CloseIcon, height: '5px', width: '5px'},
    rightIcon: {icon: CloseIcon, height: '5px', width: '5px'},
    // textColor: '#000',
    size: SIZE.md,
    onClick: undefined
}