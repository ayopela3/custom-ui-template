/*
  note for components:
  - {colorScheme}.500 is the base color which will be mostly used by the components in chakra
  - {colorScheme}.600 is the default hover color target
  - {colorScheme}.200 is the default dark mode
  - {colorScheme}.300 is the default hover color target on dark mode
*/

const colors = {
  transparent: "transparent",
  current: "currentColor",
  black: "#000000",
  white: "#FFFFFF",
  supportBlack: {
    200: "#192247",
    500: "#192247",
  },
  supportWhite: "#FAFAFA",
  supportFocused: {
    200: "#2E64C5",
    500: "#C4D8FC",
  },

  //Glow
  primaryGlow: "#24498E",
  secondaryGlow: "#3862B0",
  tertiaryGlow: "#6967CE",
  infoGlow: "#B9D8CF",
  warningGlow: "#F0D0BA",
  errorGlow: "#E2B6B8",
  alertGlow: "#FBE8BD",

  //Text or Content Color
  primaryTypography: { 200: "#F9FAFF", 500: "#0D142F" },
  secondaryTypography: { 200: "#EBEFFF", 500: "#1D2851" },
  tertiaryTypography: { 200: "#DEE5FF", 500: "#6E7DB7" },
  onColorTypography: { 200: "#0D142F", 500: "#FFFFFF" },

  //Content Background Color
  primaryBackground: { 200: "#0D142F", 500: "#F9F9FA" },
  secondaryBackground: { 200: "#324073", 500: "#EBEFFF" },
  tertiaryBackground: { 200: "#4E5C95", 500: "#C0CCFB" },
  onColorBackground: { 200: "#FAFAFA", 500: "#0D142F" },
  neutralBackground: { 200: "#1D2851", 500: "#FFFFFF" },


  //Stroke Colors
  primaryStroke: { 200: "#1D2851", 500: "#EBEFFF" },
  secondaryStroke: { 200: "#324073", 500: "#D0DAFF" },
  tertiaryStroke: { 200: "#4E5C95", 500: "#C0CCFB" },
  successStroke: { 200: "#0F5843", 500: "#B9D8CF" },
  infoStroke: { 200: "#09506E", 500: "#B6D5E2" },
  warningStroke: { 200: "#904711", 500: "#F0D0BA" },
  errorStroke: { 200: "#990C11", 500: "#E2B6B8" },
  alertStroke: { 200: "#A97E18", 500: "#FBE8BD" },

  //Icon Color
  primaryIcon: { 200: "#24498E", 500: "#507DD2" },
  secondaryIcon: { 200: "#6D9CF4", 500: "#507DD2" },
  tertiaryIcon: { 200: "#9695DD", 500: "#6967CE" },
  successIcon: { 200: "#5CA590", 500: "#167E60" },
  infoIcon: { 200: "#E7F1F5", 500: "#0D729D" },
  warningIcon: { 200: "#FAF0E8", 500: "#CE6418" },
  errorIcon: { 200: "#F5E7E7", 500: "#DE2126" },
  alertIcon: { 200: "#FEF8E9", 500: "#F1B422" },

  goldGradient: {200: "rgba(190, 151, 79, 1)", 500: "rgba(246, 232, 136, 1)"},

  primary: {
    100: "#F5F9FF",
    150: "#ECF2FE",
    200: "#D8E5FD",
    250: "#C4D8FC",
    300: "#9ABBF7",
    350: "#75A4F9",
    400: "#6097F8",
    450: "#4F8AF7",
    500: "#24498E", // altered primary
    550: "#3774E4",
    600: "#326BD3",
    650: "#3062C1",
    700: "#2D59B0",
    750: "#2A509E",
    800: "#26478E",
    850: "#233E7B",
    900: "#1F346A",
  },
  secondary: {
    100: "#F6FAFE",
    150: "#EDF5FD",
    200: "#DBEBFB",
    250: "#CBE0F8",
    300: "#A6CCF5",
    350: "#82B8F0",
    400: "#71AEED",
    450: "#60A3EB",
    500: "#4E8AF7", // altered secondary
    550: "#488ED9",
    600: "#488ED9",
    650: "#4382C8",
    700: "#3C76B8",
    750: "#345E98",
    800: "#376AA7",
    850: "#2F5287",
    900: "#233A67",
  },
  tertiary: {
    100: "#F0F0FA",
    150: "#F0F0FA",
    200: "#E1E1F6",
    250: "#D2D1F0",
    300: "#B4B3E6",
    350: "#9695DD",
    400: "#8785D9",
    450: "#7876D3",
    500: "#6967CE",
    550: "#6160C0",
    600: "#5959B4",
    650: "#5152A5",
    700: "#4A4B98",
    750: "#41458A",
    800: "#393E7D",
    850: "#313770",
    900: "#283062",
  },
  information: {
    100: "#F3F8FA",
    150: "#E7F1F5",
    200: "#CFE3EB",
    250: "#B6D5E2",
    300: "#86B9CE",
    350: "#559DBA",
    400: "#3D8EB1",
    450: "#2680A6",
    500: "#0D729D",
    550: "#125A83",
    600: "#0E6A95",
    650: "#125A83",
    700: "#10628C",
    750: "#13527A",
    800: "#144269",
    850: "#134A72",
    900: "#173358",
  },
  success: {
    100: "#F3F9F7",
    150: "#E8F2EF",
    200: "#D0E5DF",
    250: "#B9D8D0",
    300: "#8BBFB0",
    350: "#5CA590",
    400: "#469880",
    450: "#2E8B70",
    500: "#167E60",
    550: "#16755E",
    600: "#186C5B",
    650: "#166258",
    700: "#175956",
    750: "#195053",
    800: "#184751",
    850: "#183E4E",
    900: "#18354C",
  },
  warning: {
    100: "#FDF7F5",
    150: "#FBF0E8",
    200: "#F5E0D1",
    250: "#F0D1BA",
    300: "#E6B28B",
    350: "#DE945D",
    400: "#D98446",
    450: "#D4742F",
    500: "#CE6518",
    550: "#BC5E1E",
    600: "#A95922",
    650: "#985126",
    700: "#864A2B",
    750: "#75432F",
    800: "#613D34",
    850: "#5E3735",
    900: "#3D303D",
  },
  error: {
    100: "#FAF3F3",
    150: "#F5E7E6",
    200: "#ECCFCF",
    250: "#E2B6B8",
    300: "#C46D70",
    350: "#BA5658",
    400: "#B13D41",
    450: "#A72529",
    500: "#AF1B14",
    550: "#971316",
    600: "#83111D",
    650: "#751321",
    700: "#691426",
    750: "#5B182C",
    800: "#4E1A32",
    850: "#401C36",
    900: "#341E3C",
  },
  alert: {
    100: "#FEFBF4",
    150: "#FEF8E9",
    200: "#FCF0D3",
    250: "#FBE9BD",
    300: "#F8DA90",
    350: "#F6CB64",
    400: "#F5C34E",
    450: "#F2BC39",
    500: "#F1B422",
    550: "#DBA527",
    600: "#C69729",
    650: "#B0882D",
    700: "#9B7B31",
    750: "#856B34",
    800: "#6F5D38",
    850: "#5A4E3C",
    900: "#443F3F",
  },
  gray: {
    100: "#FAFAFA",
    150: "#F6F6F6",
    200: "#ECECEC",
    250: "#E2E2E2",
    300: "#D9D9D9",
    350: "#CFCFCF",
    400: "#C6C6C6",
    450: "#B8B8B8",
    500: "#A0A0A0",
    550: "#939397",
    600: "#86878E",
    650: "#797A85",
    700: "#6A6E7C",
    750: "#5D6173",
    800: "#0D142F",
    850: "#424861",
    900: "#353B58",
  },
  whiteAlpha: {
    50: "rgba(255, 255, 255, 0.04)",
    100: "rgba(255, 255, 255, 0.06)",
    200: "rgba(255, 255, 255, 0.08)",
    300: "rgba(255, 255, 255, 0.16)",
    400: "rgba(255, 255, 255, 0.24)",
    500: "rgba(255, 255, 255, 0.36)",
    600: "rgba(255, 255, 255, 0.48)",
    700: "rgba(255, 255, 255, 0.64)",
    800: "rgba(255, 255, 255, 0.80)",
    900: "rgba(255, 255, 255, 0.92)",
  },
  blackAlpha: {
    50: "rgba(0, 0, 0, 0.04)",
    100: "rgba(0, 0, 0, 0.06)",
    200: "rgba(0, 0, 0, 0.08)",
    300: "rgba(0, 0, 0, 0.16)",
    400: "rgba(0, 0, 0, 0.24)",
    500: "rgba(0, 0, 0, 0.50)",
    600: "rgba(0, 0, 0, 0.48)",
    700: "rgba(0, 0, 0, 0.64)",
    800: "rgba(0, 0, 0, 0.80)",
    900: "rgba(0, 0, 0, 0.92)",
  },
  orange: {
    50: "#FFFAF0",
    100: "#FEEBC8",
    200: "#FBD38D",
    300: "#F6AD55",
    400: "#ED8936",
    500: "#DD6B20",
    600: "#C05621",
    700: "#9C4221",
    800: "#7B341E",
    900: "#652B19",
  },
  yellow: {
    50: "#FFFFF0",
    100: "#FEFCBF",
    200: "#FAF089",
    300: "#F6E05E",
    400: "#ECC94B",
    500: "#D69E2E",
    600: "#B7791F",
    700: "#975A16",
    800: "#744210",
    900: "#5F370E",
  },
  green: {
    50: "#F0FFF4",
    100: "#C6F6D5",
    200: "#9AE6B4",
    300: "#68D391",
    400: "#48BB78",
    500: "#38A169",
    600: "#2F855A",
    700: "#276749",
    800: "#22543D",
    900: "#1C4532",
  },
  teal: {
    50: "#E6FFFA",
    100: "#B2F5EA",
    200: "#81E6D9",
    300: "#4FD1C5",
    400: "#38B2AC",
    500: "#319795",
    600: "#2C7A7B",
    700: "#285E61",
    800: "#234E52",
    900: "#1D4044",
  },
  blue: {
    50: "#ebf8ff",
    100: "#bee3f8",
    200: "#90cdf4",
    300: "#63b3ed",
    400: "#4299e1",
    500: "#3182ce",
    600: "#2b6cb0",
    700: "#2c5282",
    800: "#2a4365",
    900: "#1A365D",
  },
  cyan: {
    50: "#EDFDFD",
    100: "#C4F1F9",
    200: "#9DECF9",
    300: "#76E4F7",
    400: "#0BC5EA",
    500: "#00B5D8",
    600: "#00A3C4",
    700: "#0987A0",
    800: "#086F83",
    900: "#065666",
  },
  purple: {
    50: "#FAF5FF",
    100: "#E9D8FD",
    200: "#D6BCFA",
    300: "#B794F4",
    400: "#9F7AEA",
    500: "#805AD5",
    600: "#6B46C1",
    700: "#553C9A",
    800: "#44337A",
    900: "#322659",
  },
  pink: {
    50: "#FFF5F7",
    100: "#FED7E2",
    200: "#FBB6CE",
    300: "#F687B3",
    400: "#ED64A6",
    500: "#D53F8C",
    600: "#B83280",
    700: "#97266D",
    800: "#702459",
    900: "#521B41",
  },
  linkedin: {
    50: "#E8F4F9",
    100: "#CFEDFB",
    200: "#9BDAF3",
    300: "#68C7EC",
    400: "#34B3E4",
    500: "#00A0DC",
    600: "#008CC9",
    700: "#0077B5",
    800: "#005E93",
    900: "#004471",
  },
  facebook: {
    50: "#E8F4F9",
    100: "#D9DEE9",
    200: "#B7C2DA",
    300: "#6482C0",
    400: "#4267B2",
    500: "#385898",
    600: "#314E89",
    700: "#29487D",
    800: "#223B67",
    900: "#1E355B",
  },
  messenger: {
    50: "#D0E6FF",
    100: "#B9DAFF",
    200: "#A2CDFF",
    300: "#7AB8FF",
    400: "#2E90FF",
    500: "#0078FF",
    600: "#0063D1",
    700: "#0052AC",
    800: "#003C7E",
    900: "#002C5C",
  },
  whatsapp: {
    50: "#dffeec",
    100: "#b9f5d0",
    200: "#90edb3",
    300: "#65e495",
    400: "#3cdd78",
    500: "#22c35e",
    600: "#179848",
    700: "#0c6c33",
    800: "#01421c",
    900: "#001803",
  },
  twitter: {
    50: "#E5F4FD",
    100: "#C8E9FB",
    200: "#A8DCFA",
    300: "#83CDF7",
    400: "#57BBF5",
    500: "#1DA1F2",
    600: "#1A94DA",
    700: "#1681BF",
    800: "#136B9E",
    900: "#0D4D71",
  },
  telegram: {
    50: "#E3F2F9",
    100: "#C5E4F3",
    200: "#A2D4EC",
    300: "#7AC1E4",
    400: "#47A9DA",
    500: "#0088CC",
    600: "#007AB8",
    700: "#006BA1",
    800: "#005885",
    900: "#003F5E",
  },


  // legacy custom themes
  customGrey: {
    50: '#F7FAFC',
    100: '#EDF2F7',
    200: '#E2E8F0',
    300: '#CBD5E0',
    400: '#A0AEC0',
    500: '#F7FAFC', //default
    600: '#CBD5E0', //hover
    700: '#2D3748',
    800: '#1A202C',
    900: '#171923',
  },  
  frethanBlack: '#1B1C1E',
  frethanTransparentLight: "rgba(13, 20, 47, 0.85)",
  frethanTransparentBlack: 'rgba(0,0,0,0.5)',
  frethanGrey: '#898A94',
  frethanOrange: '#FAF0E8',
  frethanLightBlue: '#E7F1F5',
  frethanCrimsonRed: '#9D0D11',
  frethanDarkCrimsonRed: 'rgba(47, 4, 5, 1)',
  frethanPositiveGreen: '#167E60',
  frethanDarkPositiveGreen: 'rgba(7, 38, 29, 1)',
  frethanLightGrey: '#B8B8B8',
  frethanMercury: '#E7E8EA',
  frethanSoftBlue: '#4E8AF7',
  frethanSolidGrey: '#A7A7AF',
  frethanCavernPink: '#E2B6B8',
  frethanMistyRose: '#F5E7E7',
  frethanPaleAqua: '#B9D8CF',
  frethanWarningYellow: '#F1B422',
  frethanUltramarineBlue: '#3A7DF6',
  frethanDarkOrange: '#3E1E07',
  frethanDarkBlue: '#04222F',
  frethanWhite: '#FFFFFF',
  frethanDarkRed: '#2F0405',
  frethanDarkGrey: '#37373B',
  frethanLavenderBlue: '#6197F8',
  frethanVistaWhite: '#f9f9fa',
  frethanSapphireBlue: '#2958AC',
  frethanAshGrey: '#B8B9BF',
  frethanIron: '#D9D9D9',
  frethanSeashell: '#EFEFF1',
  frethanSpringWood: '#F5F5F5',
  frethanPersianRed: '#D62929',
  frethanError: '#CA151A',
  frethanDisabled: '#8799DA',
  frethanErrorText: '#E46C6E',  
  frethanDisabledDarkMode: '#6676B7',
  frethanBlueDarkMode: '#94A3D9',
  frethanAmericanBlue: '#334073',
  frethanLavenderMist: '#E5EAFF',
  frethanBlueBerry: '#538df7',
  frethanOlympicBlue: '#6196f7',
  frethanLightningYellow: '#F2BC38',
  frethanEarlyDawn: '#FFF6E2',
  frethanLightSteelBlue: '#B0BBE5',
  frethanSharpYellow: '#EEC247',
  frethanCascadeTwilight: '#24488f',

  // borders Shading

  frethanBlackLightBorder: "rgba(231, 232, 234, 1)",
  frethanBlackDarkBorder: "rgba(48, 48, 48, 1)",
  frethanCrimsonRedLightBorder: "rgba(226, 182, 184, 1)",
  frethanLightBlueBorder: "rgba(182, 213, 226, 1)",
  frethanCrimsonRedDarkBorder: "rgba(110, 9, 12, 1)",
  frethanGreenLightBorder: "rgba(185, 216, 207, 1)",
  frethanGreenDarkBorder: "rgba(15, 88, 67, 1)",
  frethanOrangeYellowBorder: "rgba(240, 208, 186, 1)",
  frethanDarkWarningYellow: "rgba(72, 54, 10, 1)",
  frethanLightYellowBorder: "rgba(251, 232, 189, 1)",
  frethanDarkYellowBorder: "rgba(169, 126, 24, 1)",
  frethanDarkOrangeBorder: "rgba(144, 71, 17, 1)",
  frethanDarkBlueBorder: "rgba(9, 80, 110, 1)",
  frethanWhiteBorder: "rgba(255, 255, 255, 0.2)",
  frethanAvatarBg: "rgba(255, 205, 102, 1)"
};

export default colors;
