import React, { FC } from "react";
import {
  Flex,
  Icon,
  useColorModeValue
} from '@chakra-ui/react'
import { Calendar, DateObject } from "react-multi-date-picker"
import ArrowBackRoundedIcon from '@mui/icons-material/ArrowBackRounded';
import ArrowForwardRounded from '@mui/icons-material/ArrowForwardRounded';
import './calendar.custom.css'


interface CalendarProp {
  numberOfMonths: number;
}


export const CalendarView: FC<CalendarProp> = ({
  numberOfMonths
}) => {
  const colorMode = useColorModeValue('light', 'dark')
  const weekDays = ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"]
  const [active, setActive] = React.useState('custom');
  const [values, setValues] = React.useState([
    new DateObject(),
    new DateObject()
  ])

  const onHandleClick = (val: any) => {
    
    setActive(val)
    if (val === 'yesterday') {
      setValues(
        [new DateObject().subtract(1, "day"),
        new DateObject().subtract(1, "day")]
      )
    }

    if (val === 'today') {
      setValues(
        [new DateObject(),
        new DateObject()]
      )
    }

    if (val === 'tomorrow') {
      setValues(
        [new DateObject().add(1, "day"),
        new DateObject().add(1, "day")]
      )
    }

    if (val === 'nextweek') {
      setValues(
        [new DateObject(),
        new DateObject().add(7, "day")]
      )
    }

    if (val === 'custom') {
      setValues(
        [new DateObject(),
        new DateObject()]
      )
    }
  }

  return (  
    <Flex>
      <Calendar
        className={colorMode === 'light' ? 'calendar-light': 'calendar-dark'}
        renderButton={(direction: any, handleClick: any) => (
              <Icon 
                onClick={handleClick}
                sx={{ 
                  padding: '5px',
                  borderRadius: '21px',
                  cursor: 'pointer'
                }}
                h={'32px'}
                w={'32px'}
              as={direction === "right" ? ArrowForwardRounded : ArrowBackRoundedIcon}
              />
        )} 
        value={values}
        range
        weekStartDayIndex={1}
        numberOfMonths={numberOfMonths}   
        weekDays={weekDays}
        showOtherDays
      /> 
      <Flex className={colorMode === 'light' ? 'custom-action-light' : 'custom-action-dark'}>
        <ul>
          <li className={active ==='yesterday' ? 'active' : ''}><a onClick={(e)=> onHandleClick('yesterday')}>Yesteday</a></li>
          <li className={active ==='today' ? 'active' : ''}><a onClick={(e)=> onHandleClick('today')}>Today</a></li>
          <li className={active ==='tomorrow' ? 'active' : ''}><a onClick={(e)=> onHandleClick('tomorrow')}>Tomorrow</a></li>
          <li className={active ==='nextweek' ? 'active' : ''}><a onClick={(e)=> onHandleClick('nextweek')}>Next Week</a></li>
          <li className={active ==='custom' ? 'active' : ''}><a  onClick={(e)=> onHandleClick('custom')}>Custom</a></li>
        </ul>
      </Flex>
    </Flex>
    
  );
};

CalendarView.defaultProps = {
  numberOfMonths: 2,
};
