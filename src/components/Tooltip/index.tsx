import React, { FC, ReactElement, ReactNode } from "react";
import {
  Box,
  IconButton,
  PlacementWithLogical,
  Tooltip as ChakraTooltip,
  Text,
  TooltipProps,
  Button,
  useColorMode,
} from "@chakra-ui/react";
import { CloseIcon } from "@chakra-ui/icons";

interface InputFieldProps {
  title: string;
  description?: ReactNode | string;
  children: ReactNode | ReactElement;
  placement?: PlacementWithLogical;
  isOpen?: boolean;
  closeTooltip?: () => void;
  tooltipProps?: TooltipProps;
  isTour?: boolean;
  tourDescription?: string | ReactNode;
  fitContent?: boolean;
  onNext?: () => void;
  skipTour?: () => void;
}

export const Tooltip:FC<InputFieldProps> = ({
  title,
  description,
  placement,
  isOpen,
  children,
  closeTooltip,
  isTour,
  tooltipProps,
  tourDescription,
  fitContent,
  onNext,
  skipTour,
}: InputFieldProps) => {
  const { colorMode } = useColorMode();
  const CustomLabel = () => (
    <Box paddingBottom={isTour? 1 : 0}>
      <Box display="flex" justifyContent="space-between">
        <Text fontSize="md">{title}</Text>
        <IconButton
          aria-label="close"
          size="xs"
          variant="ghost"
          icon={<CloseIcon />}
          onClick={closeTooltip}
          color={colorMode === "light" ? "frethanWhite" : "primaryBackground.200"}
        />
      </Box>
      <Text fontSize="sm">{description}</Text>
      {isTour && (
        <Box display="flex" gap={2} alignItems="center">
          <Text fontSize="xs" style={{ fontSize: 11 }}>
            {tourDescription}
          </Text>
          -
          <Box
            as="button"
            fontSize={11}
            textDecoration="underline"
            onClick={skipTour}
          >
            Skip Tour
          </Box>
          <Button
            onClick={onNext}
            size={"xs"}
            color="primaryBackground.200"
            backgroundColor="#fff"
          >
            Next
          </Button>
        </Box>
      )}
    </Box>
  );

  return (
    <ChakraTooltip
      arrowSize={8}
      hasArrow
      borderRadius={4}
      label={<CustomLabel />}
      placement={placement}
      {...tooltipProps}
      isOpen={isOpen}
      minWidth={fitContent? "none" : 141}
      bgColor={colorMode === "light" ? "primaryBackground.200" : "frethanWhite"}
    >
      {children}
    </ChakraTooltip>
  );
};
