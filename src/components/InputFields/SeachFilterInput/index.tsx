import { CategoryItemProps } from '@/lib/types'
import { SearchIcon } from '@chakra-ui/icons'
import {
  Flex,
  Input,
  InputGroup,
  InputLeftElement,
  Icon,
  Select,
  Button,
  HStack,
  BoxProps
} from '@chakra-ui/react'

interface SearchFilterInputProps {
  containerProps?: Omit<BoxProps, 'maxW'>
  categoriesList?: CategoryItemProps[]
  onCategoryChange?: (categoryId: number) => void
  catValue?: number
}

const SearchFilterInput = ({
  containerProps,
  categoriesList,
  onCategoryChange,
  catValue
}: SearchFilterInputProps) => {
  const onChange = (value: string) => {
    if (value && onCategoryChange) {
      onCategoryChange((Number(value)))
    }
  }
  return (
    <HStack
      alignItems="center"
      spacing={4}
      {...containerProps}
    >
      <Flex
        alignItems="center"
        minW="567px"
      >
        <InputGroup>
          <InputLeftElement>
            <Icon
              as={SearchIcon}
              color="primaryGlow"
              w="18.33px"
              h="18.33px"
            />
          </InputLeftElement>
          <Input
            placeholder="Search..."
            color="tertiaryTypography.500"
            fontWeight="500"
            fontSize="14px"
            lineHeight="16px"
            borderTopRightRadius="0"
            borderBottomRightRadius="0"
            minW=""
          />
        </InputGroup>
        <Select
          placeholder="Categories"
          fontWeight="500"
          fontSize="14px"
          lineHeight="16px"
          color="tertiaryTypography.500"
          w="450px"
          borderTopLeftRadius="0"
          borderBottomLeftRadius="0"
          borderLeft="none"
          value={catValue}
          onChange={(event) => onChange(event?.target?.value)}
        >
          {
            categoriesList && !!categoriesList.length && categoriesList.map((details) => {
              return (
                <option
                  key={details.id}
                  value={details.id}
                >
                  {details.name}
                </option>
              )
            })
          }
        </Select>
      </Flex>
      <Button
        bgColor="primaryGlow"
        color="frethanWhite"
        borderRadius="8px"
        w="139px"
        h="40px"
        fontWeight="500"
        fontSize="16px"
        lineHeight="20px"
        _hover={{
          bgColor: 'primaryGlow'
        }}
      >
        Search
      </Button>
    </HStack>
  )
}

export default SearchFilterInput