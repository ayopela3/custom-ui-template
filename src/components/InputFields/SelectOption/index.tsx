import { OptionsProps, SelectFieldVariant } from '@/lib/types'
import {
  Select, 
  SelectProps
} from '@chakra-ui/react'

interface SelectOptionProps {
  selectProps?: SelectProps
  options?: OptionsProps[] | []
  variant?: SelectFieldVariant
}

const SelectOption = ({
  selectProps,
  options,
  variant
}: SelectOptionProps) => {
  const defaultVariant = variant || 'plain'

  return (
    <Select
      {...selectProps} 
      variant={defaultVariant}
    >
      {
        options && !!options.length && (
          options.map((details, key) => (
            <option key={key} value={details.value}>{details.name}</option>
          ))
        )
      }
    </Select>
  )
}

export default SelectOption