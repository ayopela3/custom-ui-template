import React, { FC, ReactElement, useEffect, useMemo, useState } from 'react'
import { UploadIcon } from '@/components/icons'
import colors from '@/components/themes/colors'
import { InputFieldVariant } from '@/lib/types'
import { getIconColor, getLeftIcon } from '@/lib/utils'
import {
  Box,
  Flex,
  Icon,
  InputGroup,
  InputLeftElement,
  Progress,
  Stack,
  Text,
  useColorMode,
  useColorModeValue,
} from '@chakra-ui/react'
import { Cancel, CheckCircle, AttachFile } from '@mui/icons-material'
import { DEFAULT_INPUT_FIELD_WIDTH } from '@/constants/forms'
import { useDropzone } from 'react-dropzone'
import Image from 'next/image'

export enum UploaderType {
  DROPZONE = 'DROPZONE',
  INPUTFIELD = 'INPUTFIELD',
}

interface DropZoneProps {
  width: number
  height: number
  backgroundColor?: string
  dropZoneConfig: object
  fileSizeLimit: number
  maxFiles?: number
  isLoading?: boolean
  onUpload?: Function
  type: UploaderType
  label?: string
  isOptional?: boolean
  description?: string
  helperText?: string
  variant?: InputFieldVariant
  placeholder?: string
  isIndeterminate: boolean
  loadingValue?: number
  isRequired: boolean
  rule: ReactElement
}

export const DropZone: FC<DropZoneProps> = ({
  width,
  height,
  dropZoneConfig,
  onUpload = () => {},
  fileSizeLimit = 0,
  maxFiles,
  isLoading = false,
  type,
  label = '',
  isOptional,
  description = '',
  helperText = '',
  variant,
  placeholder = '',
  isIndeterminate,
  isRequired,
  loadingValue = 0,
  rule,
}) => {
  const [iconStatus, setIconStatus]: any = useState(null)
  const [files, setFiles]: any = useState([])
  const currentVariant = variant || 'plain'
  const { colorMode } = useColorMode()
  const leftIconColor = getIconColor(currentVariant)
  const leftIcon = getLeftIcon(currentVariant)
  function formatBytes(bytes: number, decimals = 2) {
    if (!+bytes) return '0 Bytes'
    const k = 1024
    const dm = decimals < 0 ? 0 : decimals
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
    const i = Math.floor(Math.log(bytes) / Math.log(k))
    return `${parseFloat((bytes / Math.pow(k, i)).toFixed(dm))} ${sizes[i]}`
  }

  const fileSizeValidator = (file: File) => {
    if (file.size > fileSizeLimit * 1000000) {
      setIconStatus('error')
      return {
        code: 'filesize-too-large',
        message: `file size has exceeded the allowable size of ${formatBytes(
          fileSizeLimit,
          2
        )}`,
      } as any
    }
  }

  const { acceptedFiles, fileRejections, getRootProps, getInputProps } =
    useDropzone({
      ...dropZoneConfig,
      maxFiles: maxFiles,
      validator: fileSizeValidator,
      onDrop: (acceptedFiles) => {
        onUpload(acceptedFiles)
        setFiles(
          acceptedFiles.map((file: File) =>
            Object.assign(file, {
              preview: URL.createObjectURL(file),
            })
          )
        )
      },
    })

  useEffect(() => {
    if (acceptedFiles.length !== 0) {
      setIconStatus('success')
    } else if (fileRejections.length !== 0) {
      setIconStatus('error')
    } else {
      setIconStatus(null)
    }
  }, [acceptedFiles, fileRejections])

  const thumbnailDisplay = useMemo(() => {
    const removeFile = (file: any) => {
      const newFiles = [...files]
      let index = newFiles.indexOf(file)
      newFiles.splice(index, 1)
      setIconStatus(null)
      setFiles(newFiles)
    }

    const display = files.map((file: any) => (
      <Box key={file.name} style={{ marginLeft: 8 }}>
        <Image
          alt={'thumbnail'}
          width={64}
          height={64}
          src={file.preview}
          style={{ width: 64, height: 64, borderRadius: 8 }}
          onLoad={() => {
            URL.revokeObjectURL(file.preview)
          }}
        />
        <Icon
          style={{
            color: colors.frethanCrimsonRed,
            position: 'absolute',
            zIndex: 24,
            marginLeft: -12,
            marginTop: -4,
            backgroundColor: '#FFF',
            borderRadius: 24,
          }}
          as={Cancel}
          onClick={() => {
            removeFile(file)
          }}
        />
      </Box>
    ))
    return display
  }, [files])

  const IconDisplay = useMemo(() => {
    if (iconStatus === null) return <Icon as={UploadIcon} />
    else if (iconStatus === 'error')
      return <Icon style={{ color: colors.frethanCrimsonRed }} as={Cancel} />
    else
      return (
        <Icon style={{ color: colors.frethanPositiveGreen }} as={CheckCircle} />
      )
  }, [iconStatus])

  return type === UploaderType.DROPZONE ? (
    <>
      <div
        style={{
          width,
          height,
          backgroundColor: colorMode !== 'dark' ? '#FFF' : '#1D2851',
          borderRadius: 8,
          borderStyle: 'dashed',
          borderSpacing: 2,
          borderWidth: 2,
          borderColor: colorMode !== 'dark' ? '#b8b9bf' : '#324073',
        }}
      >
        <Box
          style={{ textAlign: 'center', paddingTop: 24 }}
          {...getRootProps({ className: 'dropzone' })}
        >
          {IconDisplay}
          {isLoading === false ? (
            <>
              <input {...getInputProps()} />
              <Text
                fontSize={'small'}
                fontWeight={'medium'}
                style={{ textAlign: 'center', marginTop: 12, marginBottom: 24 }}
              >
                Drag & Drop file here <br />
                <div style={{ flexDirection: 'row', textAlign: 'left' }}>
                  <Stack style={{ justifyContent: 'center' }} direction={'row'}>
                    <Text>or</Text>
                    <Text color={'blue.400'} decoration={'underline'}>
                      upload a file
                    </Text>
                    <Text>from your computer</Text>
                  </Stack>
                </div>
              </Text>
              <Text fontSize={'smaller'}>
                {rule}
                {formatBytes(fileSizeLimit)} in size
              </Text>
            </>
          ) : (
            <>
              <Progress
                size="sm"
                sx={{
                  '& > div': {
                    background:
                      colorMode === 'light'
                        ? 'linear-gradient(90deg, #F6E888 10%, #BE974F 90%)'
                        : colors.green[400],
                    borderRadius: 12,
                  },
                }}
                style={{
                  marginLeft: 'auto',
                  marginRight: 'auto',
                  marginTop: 48,
                  marginBottom: 24,
                  background: 'rgba(120,120,120,0.3)',
                  borderRadius: 8,
                }}
                isIndeterminate={isIndeterminate}
                value={loadingValue}
                width={width / 2}
              />
              <Text fontSize={'small'} fontWeight={'medium'}>
                Uploading...
              </Text>
            </>
          )}
        </Box>
        <br />
      </div>
    </>
  ) : (
    <>
      <Box>
        <Box mb="8px">
          <Text variant="labelMed" mb="4px">
            {label} {!!isOptional && <Text as="span">(optional)</Text>}
            {!!isRequired && (
              <Text
                as={'span'}
                color={'red'}
                paddingLeft={isOptional ? 200 : 264}
              >
                *
              </Text>
            )}
          </Text>
          <Text variant="captionMed">{description}</Text>
        </Box>
        <InputGroup {...getRootProps({ className: 'dropzone' })}>
          <InputLeftElement>{IconDisplay}</InputLeftElement>
          <Box
            style={{
              width: DEFAULT_INPUT_FIELD_WIDTH + 40,
              height: 40,
              borderRadius: 8,
              borderWidth: 1,
              borderStyle: 'dashed',
              borderColor: '#rgba(0,0,0,0.4)',
            }}
          >
            <Text style={{ marginLeft: 48, marginTop: 6 }}>{placeholder}</Text>
            <input {...getInputProps()} />
          </Box>
          <Icon
            as={AttachFile}
            style={{
              marginTop: 8,
              transform: 'rotate(45deg)',
              marginLeft: -36,
            }}
          />
        </InputGroup>
        {helperText && helperText !== '' && (
          <Text variant="captionMed" color="frethanGrey" mt="4">
            {helperText}
          </Text>
        )}
        <Flex
          direction={'row'}
          style={{ maxWidth: 200, marginTop: 12, marginLeft: -8 }}
        >
          {thumbnailDisplay}
        </Flex>
      </Box>
    </>
  )
}

DropZone.defaultProps = {
  width: 480,
  height: 186,
  backgroundColor: 'rgba(180,180,180, 0.1)',
  dropZoneConfig: {
    accept: {
      'image/jpeg': ['.jpeg'],
      'image/jpg': ['.jpg'],
      'image/png': ['.png'],
    },
  },
  maxFiles: 1,
  fileSizeLimit: 10,
  isLoading: false,
  isRequired: true,
  type: UploaderType.DROPZONE,
  label: '',
  placeholder: '',
  description: '',
  helperText: '',
  isIndeterminate: false,
  loadingValue: 70,
  rule: (
    <>
      Only <b>JPG</b>, <b>JPEG</b> or <b>PNG</b> are allowed up to{' '}
    </>
  ),
}
