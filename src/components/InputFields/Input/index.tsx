/* eslint-disable @next/next/no-img-element */
import { DEFAULT_INPUT_FIELD_VARIANT } from "../../../constants/forms";
import { CountryType, InputFieldVariant, InputIconProps } from "../../../lib/types";
import { ChevronDownIcon } from "@chakra-ui/icons";
import {
  InputGroup,
  InputLeftElement,
  Input,
  InputProps,
  Icon,
  InputRightElement,
  InputGroupProps,
  Menu,
  MenuButton,
  MenuList,
  MenuItem,
} from "@chakra-ui/react";
import React from "react";

interface FieldInputProps {
  placeholder?: string
  type: "text" | "password" | "number"
  inputGroupProps?: InputGroupProps
  inputProps?: InputProps
  variant?: InputFieldVariant
  leftIcon?: InputIconProps
  rightIcon?: InputIconProps
  countries?: readonly CountryType[]
  onRightIconClick?: () => void
  onLeftIconClick?: () => void
}

export const TextFieldInput = ({
  placeholder,
  type,
  inputProps,
  variant,
  leftIcon,
  rightIcon,
  inputGroupProps,
  countries,
  onRightIconClick,
  onLeftIconClick
}: FieldInputProps) => {
  return (
    <InputGroup {...inputGroupProps}>
      {leftIcon && leftIcon.icon && (
        <InputLeftElement
          _hover={{
            cursor: onLeftIconClick ? 'pointer' : 'unset'
          }}
          onClick={onLeftIconClick}
        >
          <Icon
            as={leftIcon.icon}
            color={leftIcon.color || "frethanBlack"}
            h={leftIcon.height}
            w={leftIcon.width}
            ml="5px"
          />
        </InputLeftElement>
      )}
      {countries && (
        <InputLeftElement width={50}>
          <Menu>
            <MenuButton transition="all 0.2s">
              <img
                style={{ display: "inline" }}
                loading="lazy"
                width="18"
                src={`https://flagcdn.com/w20/ph.png`}
                srcSet={`https://flagcdn.com/w40/ph.png 2x`}
                alt=""
              />{" "}
              <ChevronDownIcon />
            </MenuButton>
            <MenuList>
              {countries.map((option, idx) => (
                <MenuItem key={idx}>
                  <img
                    loading="lazy"
                    width="20"
                    src={`https://flagcdn.com/w20/${option.code.toLowerCase()}.png`}
                    srcSet={`https://flagcdn.com/w40/${option.code.toLowerCase()}.png 2x`}
                    alt=""
                  />
                  {option.label} ({option.code}) +{option.phone}
                </MenuItem>
              ))}
            </MenuList>
          </Menu>
        </InputLeftElement>
      )}
      <Input
        variant={variant || DEFAULT_INPUT_FIELD_VARIANT}
        placeholder={placeholder}
        type={type}
        padding="16px 42px"
        fontSize="14px"
        fontWeight="500"
        lineHeight="16px"
        borderRadius="8px"
        {...inputProps}
      />
      {rightIcon && rightIcon.icon && (
        <InputRightElement
          _hover={{
            cursor: onRightIconClick ? 'pointer' : 'unset'
          }}
          onClick={onRightIconClick}
        >
          <Icon
            onClick={rightIcon.action}
            as={rightIcon.icon}
            color={rightIcon.color || "frethanBlack"}
            h={rightIcon.height}
            w={rightIcon.width}
            ml="5px"
          />
        </InputRightElement>
      )}
    </InputGroup>
  );
};
