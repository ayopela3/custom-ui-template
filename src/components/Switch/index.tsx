import React, { FC } from "react";
import { Switch as ChakraSwitch } from "@chakra-ui/react";
import { SwitchSizes } from "../../lib/types";

interface LoadersPropTypes {
  size: SwitchSizes;
  disabled: boolean;
}
// Cannot set backgroundColor to thumb when disabled
export const Switch:FC<LoadersPropTypes> = ({ size, disabled }: LoadersPropTypes) => {
  return (
    <ChakraSwitch
      variant={disabled? "disabled" : "plain"}
      disabled={disabled}
      size={size}
    />
  );
};
