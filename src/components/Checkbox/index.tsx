import React, { FC, useMemo } from "react";
import {
  Checkbox, CheckboxProps
} from '@chakra-ui/react'
import { CheckIconBox } from "../../icons"
import { CheckboxSizes, CheckboxVariants } from "../../lib/types"
import { getCheckboxIconColor, getCheckboxIconSize } from "../../lib/utils"
import colors from "../../components/themes/colors"

  interface CheckboxesProps {
    label?: string
    size?: CheckboxSizes
    variant?: CheckboxVariants
    checkboxProps?: CheckboxProps
  }
  
  export const CheckboxComponent: FC<CheckboxesProps> = ({
    label,
    size,
    variant,
    checkboxProps
  }) => {
    const currentCheckboxSize: CheckboxSizes = size || 'md'
    const currentVariant: CheckboxVariants = variant || 'plain'
    const checkColor = currentVariant === 'disabled' ? colors.frethanGrey : colors.frethanWhite
    const iconSize = useMemo(() => {
      return getCheckboxIconSize(currentCheckboxSize)
    }, [currentCheckboxSize])
    const checkboxIconColor = useMemo(() => {
      return getCheckboxIconColor(currentVariant)
    }, [currentVariant])

    return (
      <Checkbox 
        icon={
          <CheckIconBox 
            h={iconSize} 
            w={iconSize}
            checkBgColor={checkboxIconColor}
            checkColor={checkColor}
          />
        }
        size={currentCheckboxSize}
        variant={currentVariant}
        {...checkboxProps}
      >
        {label}
      </Checkbox>
    )
  }