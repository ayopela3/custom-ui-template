import { ComponentWithAs, Icon, IconProps } from '@chakra-ui/react';
import React from 'react';
export const EmailIcon: ComponentWithAs<'svg', IconProps> = (props: IconProps) => (
  <Icon viewBox={`0 0 44 36`} fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
    <path fillRule="evenodd" clipRule="evenodd" d="M0 6C0 2.68629 2.68629 0 6 0H38C41.3137 0 44 2.68629 44 6V30C44 33.3137 41.3137 36 38 36H6C2.68629 36 0 33.3137 0 30V6ZM6 4C4.89543 4 4 4.89543 4 6V8.86762L22 19.6676L40 8.86762V6C40 4.89543 39.1046 4 38 4H6ZM40 13.5324L24.058 23.0976C22.7913 23.8576 21.2087 23.8576 19.942 23.0976L4 13.5324V30C4 31.1046 4.89543 32 6 32H38C39.1046 32 40 31.1046 40 30V13.5324Z" 
    fill="currentColor"/>
  </Icon>
);
