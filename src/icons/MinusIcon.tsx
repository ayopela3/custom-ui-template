import { ComponentWithAs, Icon, IconProps } from '@chakra-ui/react';
import React from 'react';
export const MinusIcon: ComponentWithAs<'svg', IconProps> = (props: IconProps) => (
  <Icon viewBox={`0 0 12 2`} xmlns="http://www.w3.org/2000/svg" {...props}>
    <path fillRule="evenodd" clipRule="evenodd" d="M0.664063 0.999995C0.664063 0.631805 0.962539 0.333328 1.33073 0.333328L10.6641 0.333328C11.0323 0.333328 11.3307 0.631805 11.3307 0.999995C11.3307 1.36818 11.0323 1.66666 10.6641 1.66666L1.33073 1.66666C0.962539 1.66666 0.664063 1.36818 0.664063 0.999995Z" 
    fill="currentColor"/>
  </Icon>
);
