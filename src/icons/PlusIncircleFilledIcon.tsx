import { ComponentWithAs, Icon, IconProps } from '@chakra-ui/react';
import React from 'react';
export const PlusIncircleFilledIcon: ComponentWithAs<'svg', IconProps> = (props: IconProps) => (
  <Icon viewBox={`0 0 21 20`} fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
    <path fillRule="evenodd" clipRule="evenodd" d="M10.5 20C16.0228 20 20.5 15.5228 20.5 10C20.5 4.47715 16.0228 0 10.5 0C4.97715 0 0.5 4.47715 0.5 10C0.5 15.5228 4.97715 20 10.5 20ZM14.6667 10.8333C15.1269 10.8333 15.5 10.4602 15.5 9.99992C15.5 9.53969 15.1269 9.16659 14.6667 9.16659H11.3333V5.83333C11.3333 5.3731 10.9602 5 10.5 5C10.0398 5 9.66667 5.3731 9.66667 5.83333V9.16659H6.33333C5.8731 9.16659 5.5 9.53968 5.5 9.99992C5.5 10.4602 5.8731 10.8333 6.33333 10.8333H9.66667V14.1667C9.66667 14.6269 10.0398 15 10.5 15C10.9602 15 11.3333 14.6269 11.3333 14.1667V10.8333H14.6667Z" 
    fill="currentColor"/>
  </Icon>
);
