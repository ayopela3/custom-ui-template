  
  export interface OrderProps {
    id: number | string
    orderId: string
    customer_name: string
    order: OrderDetailProps
    delivery_date: Date
    status: number
  }
  
  export interface StatusColorProps {
    id: number | string
    name: string
    color: string
    bgColor: string
  }
  
  export interface OrderDetailProps {
    id: number
    category: number
    subCategory: number
    categories: {name: string, id: number}[]
    productName: string
    company_name: string
    country_from: string
    productRange: number
    currency: number
    quantity: number
    unitType: number
    unitPrice: number
    requirementsDetails: string
    files: File[]
    paymentType?: number
  }