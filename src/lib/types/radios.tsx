export type RadiosSizes = "md" | "lg" | "sm"
export type RadioVariants = 'plain' | 'solid' | 'glow' | 'danger' | 'disabled'
