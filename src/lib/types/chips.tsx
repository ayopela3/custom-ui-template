export type ChipsSizes = "sm" | "md" | "lg";
export type ChipsVariant = "neutral" | "success" | "error" | "warning" | "alert" | "information";
