export type CheckboxSizes = 'lg' | 'md' | 'sm'

export type CheckboxVariants = 'plain' | 'solid' | 'glow' | 'danger' | 'disabled'