import { HyperlinkVariants } from '../lib/types'

export const hypertextSizes = {
  xxl: {
    textProp: {
      fontSize: '18px',
      lineHeight: '24px',
    },
    arrowProp: {
      width: '18px',
      height: '16px',
    },
  },
  xl: {
    textProp: {
      fontSize: '16px',
      lineHeight: '20px',
    },
    arrowProp: {
      width: '15px',
      height: '13.33px',
    },
  },
  md: {
    textProp: {
      fontSize: '14px',
      lineHeight: '16px',
    },
    arrowProp: {
      width: '12px',
      height: '10.67px',
    },
  },
  sm: {
    textProp: {
      fontSize: '12px',
      lineHeight: '14px',
    },
    arrowProp: {
      width: '9px',
      height: '8x',
    },
  },
}

export const hypertextColors = {
  [HyperlinkVariants.HYPERLINK_NORMAL]: 'frethanLavenderBlue',
  [HyperlinkVariants.HYPERLINK_SOLID]: 'frethanSoftBlue',
  [HyperlinkVariants.HYPERLINK_GLOW]: 'frethanSoftBlue',
  [HyperlinkVariants.HYPERLINK_DARK]: 'frethanSapphireBlue',
}
