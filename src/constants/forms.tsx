import { BadgeVariant, CategoriesProps, OptionsProps, TagProps } from "../lib/types"

export const DEFAULT_ERROR_MESSAGE = '*this field is required'
export const DEFAULT_INPUT_FIELD_WIDTH = 300
export const DEFAULT_INPUT_FIELD_VARIANT = 'default'
export const INPUT_GROUP_FIELD_PROPS = {
  LG: {
    buttonHeight: '40px',
    inputHeight: '48px'
  },
  MD: {
    buttonHeight: '32px',
    inputHeight: '40px'
  },
  SM: {
    buttonHeight: '24px',
    inputHeight: '32px'
  }
}
export const STEPPER_FIELD_PROPS = {
  LG: {
    buttonProps: '40px',
    inputHeight: '48px'
  },
  MD: {
    buttonProps: '32px',
    inputHeight: '40px'
  },
  SM: {
    buttonProps: '24px',
    inputHeight: '32px'
  }
}
export const INPUT_GROUP_SIZES = {
  LG: 'lg',
  MD: 'md',
  SM: 'sm'
}

export const BADGES_VARIANTS = {
  PLAIN: {
    COLOR: 'frethanBlack',
    BORDER: '1px solid',
    BORDER_COLOR: 'frethanAshGrey',
    BACKGROUND_COLOR: 'frethanWhite',
    BORDER_RADIUS: '6px',
    PADDING: '12px 0 12px 12px',
    BOX_SHADOW: 'none'
  },
  SOLID: {
    COLOR: 'frethanBlack',
    BORDER: '1px solid',
    BORDER_COLOR: 'frethanSoftBlue',
    BACKGROUND_COLOR: 'frethanWhite',
    BORDER_RADIUS: '6px',
    PADDING: '12px 0 12px 12px',
    BOX_SHADOW: 'none'
  },
  GLOW: {
    COLOR: 'frethanBlack',
    BORDER: '1px solid',
    BORDER_COLOR: 'frethanSoftBlue',
    BACKGROUND_COLOR: 'frethanWhite',
    BORDER_RADIUS: '6px',
    PADDING: '12px 0 12px 12px',
    BOX_SHADOW: '0 0 3px 3px rgba(78, 138, 247, 0.4)'
  },
  DANGER: {
    COLOR: 'frethanBlack',
    BORDER: '1px solid',
    BORDER_COLOR: 'frethanCavernPink',
    BACKGROUND_COLOR: 'frethanMistyRose',
    BORDER_RADIUS: '6px',
    PADDING: '12px 0 12px 12px',
    BOX_SHADOW: 'none'
  },
  DISABLED: {
    COLOR: 'frethanGrey',
    BORDER: '1px solid',
    BORDER_COLOR: 'frethanMercury',
    BACKGROUND_COLOR: 'frethanMercury',
    BORDER_RADIUS: '6px',
    PADDING: '12px 0 12px 12px',
    BOX_SHADOW: 'none'
  }
}

export const INPUT_BADGE_VARIANTS = {
  [BadgeVariant.PLAIN]: {

  }
}

export const CATEGORIES: CategoriesProps[] = [
  {
    id: 1,
    abbrev: 'AU',
    mobileRegionCode: '+61',
    name: 'Australia',
    mobileName: 'Australia +61'
  },
  {
    id: 2,
    abbrev: 'CN',
    mobileRegionCode: '+86',
    name: 'China',
    mobileName: 'China +86'
  }
]

export const SAMPLE_PROVINCE: OptionsProps[] = [
  {
    name: 'Province 1',
    value: 1
  },
  {
    name: 'Province 2',
    value: 2
  },
  {
    name: 'Province 3',
    value: 3
  },
]

export const SAMPLE_CITY: OptionsProps[] = [
  {
    name: 'City 1',
    value: 1
  },
  {
    name: 'City 2',
    value: 2
  },
  {
    name: 'City 3',
    value: 3
  },
]

export const SAMPLE_COUNTRY: OptionsProps[] = [
  {
    name: 'Country 1',
    value: 1
  },
  {
    name: 'Country 2',
    value: 2
  },
  {
    name: 'Country 3',
    value: 3
  },
]

export const SAMPLE_SUPPLIERS_TAG_OPTIONS: TagProps[] = [
  {
    name: 'Supplier 1',
    value: 1
  },
  {
    name: 'Supplier 2',
    value: 2
  },
  {
    name: 'Supplier 3',
    value: 3
  },
  {
    name: 'Supplier 4',
    value: 4
  },
  {
    name: 'Supplier 5',
    value: 5
  },
  {
    name: 'Supplier 6',
    value: 6
  },
  {
    name: 'Supplier 7',
    value: 7
  },
  {
    name: 'Supplier 8',
    value: 8
  },
  {
    name: 'Supplier 9',
    value: 9
  },
  {
    name: 'Supplier 10',
    value: 10
  }
]

export const SAMPLE_SIZES: OptionsProps[] = [
  {
    name: 'Extra Small',
    value: 'xs'
  },
  {
    name: 'Small',
    value: 's'
  },{
    name: 'Medium',
    value: 'm'
  },
  {
    name: 'Large',
    value: 'l'
  },
  {
    name: 'Extra Large',
    value: 'xl'
  },
  {
    name: 'Double Extra Large',
    value: 'xxl'
  },
  {
    name: 'Custom',
    value: 'custom'
  },
  
  
]

export const SAMPLE_COLORS: OptionsProps[] = [
  {
    name: 'Red',
    value: 'Red'
  },
  {
    name: 'Orange',
    value: 'Orange',
  },
  {
    name: 'Blue',
    value: 'Blue'
  },{
    name: 'Yellow',
    value: 'Yellow'
  },
  {
    name: 'Green',
    value: 'Green'
  },
  {
    name: 'Black',
    value: 'Black'
  },
  {
    name: 'White',
    value: 'White'
  },
]