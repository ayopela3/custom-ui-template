export interface LinkProps {
  title: string
  address: string
  isExternal: boolean
}

export interface StateButtonProps {
  onClick?: () => void
  title?: string
}