import { ReactElement } from "react"

export interface TabsLabel {
  title: string
  count?: number
}

export interface TabsData {
  label: TabsLabel
  content?: ReactElement
}