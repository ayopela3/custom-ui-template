export enum HyperlinkSizes {
  XXL = 'xxl',
  XL = 'xl',
  MD = 'md',
  SM = 'sm',
}

export enum HyperlinkVariants {
  HYPERLINK_NORMAL = 'hyperlinkNormal',
  HYPERLINK_SOLID = 'hyperlinkSolid',
  HYPERLINK_GLOW = 'hyperlinkGlow',
  HYPERLINK_DARK = 'hyperlinkDark',
}

export type HyperlinkTextVariants =
  | 'hyperlinkNormal'
  | 'hyperlinkSolid'
  | 'hyperlinkGlow'
  | 'hyperlinkDark'

export enum HyperlinkVariantsColor {
  hyperlinkNormal = 'frethanLavenderBlue',
  hyperlinkSolid = 'frethanSoftBlue',
  hyperlinkGlow = 'frethanSoftBlue',
  hyperlinkDark = 'frethanSapphireBlue',
}

export type HyperlinkTextSize = 'xxl' | 'xl' | 'md' | 'sm'
