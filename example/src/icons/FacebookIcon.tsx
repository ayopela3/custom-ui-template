import { ComponentWithAs, Icon, IconProps } from '@chakra-ui/react';
import React from 'react';
export const FacebookIcon: ComponentWithAs<'svg', IconProps> = (props: IconProps) => (
  <Icon viewBox={`0 0 18 18`} xmlns="http://www.w3.org/2000/svg" {...props}>
    <path d="M9 0.25C4.16699 0.25 0.25 4.19085 0.25 9.0533C0.25 13.4481 3.44922 17.0898 7.63281 17.75V11.598H5.41113V9.0533H7.63281V7.11382C7.63281 4.90784 8.93848 3.68879 10.938 3.68879C11.895 3.68879 12.8965 3.86073 12.8965 3.86073V6.02717H11.7925C10.7056 6.02717 10.3672 6.70633 10.3672 7.40268V9.0533H12.7939L12.406 11.598H10.3672V17.75C14.5508 17.0898 17.75 13.4481 17.75 9.0533C17.75 4.19085 13.833 0.25 9 0.25Z"
      fill="#1877F2" />
  </Icon>
);
