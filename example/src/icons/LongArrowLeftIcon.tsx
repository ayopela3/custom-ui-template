import { ComponentWithAs, Icon, IconProps } from '@chakra-ui/react';
import React from 'react';
export const LongArrowLeftIcon: ComponentWithAs<'svg', IconProps> = (props: IconProps) => (
  <Icon viewBox={`0 0 16 16`} fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
    <path fillRule="evenodd" clipRule="evenodd" d="M5.13807 4.19526C5.39842 4.45561 5.39842 4.87772 5.13807 5.13807L2.94281 7.33333H14.6667C15.0349 7.33333 15.3333 7.63181 15.3333 8C15.3333 8.36819 15.0349 8.66667 14.6667 8.66667H2.94281L5.13807 10.8619C5.39842 11.1223 5.39842 11.5444 5.13807 11.8047C4.87772 12.0651 4.45561 12.0651 4.19526 11.8047L0.86193 8.4714C0.601581 8.21106 0.601581 7.78894 0.86193 7.5286L4.19526 4.19526C4.45561 3.93491 4.87772 3.93491 5.13807 4.19526Z"
      fill="currentColor" />
  </Icon>
);
