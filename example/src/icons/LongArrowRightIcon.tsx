import { ComponentWithAs, Icon, IconProps } from '@chakra-ui/react';
import React from 'react';
export const LongArrowRightIcon: ComponentWithAs<'svg', IconProps> = (props: IconProps) => (
  <Icon viewBox={`0 0 16 16`} fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
    <path d="M10.8619 10.8619C10.6016 11.1223 10.6016 11.5444 10.8619 11.8047C11.1223 12.0651 11.5444 12.0651 11.8047 11.8047L15.1381 8.47141C15.3984 8.21106 15.3984 7.78895 15.1381 7.5286L11.8047 4.19526C11.5444 3.93491 11.1223 3.93491 10.8619 4.19526C10.6016 4.45561 10.6016 4.87772 10.8619 5.13807L13.0572 7.33333H1.33332C0.965133 7.33333 0.666656 7.63181 0.666656 8C0.666656 8.36819 0.965133 8.66667 1.33332 8.66667H13.0572L10.8619 10.8619Z" 
    fill="currentColor"/>
  </Icon>
);
