import { FC, useState } from 'react'
import {
  Slider,
  SliderTrack,
  SliderFilledTrack,
  SliderThumb,
  useColorModeValue,
} from '@chakra-ui/react'
import { Tooltip } from '../Tooltip'

interface SliderValueTypes {
  title: string
  description: string
  value: number
}

interface SliderProp {
  sliderValue: SliderValueTypes
  isDisabled: boolean
  min: number
  max: number
}

export const Sliders: FC<SliderProp> = ({
  sliderValue,
  isDisabled = false,
  min,
  max,
}) => {
  const mainBackground = useColorModeValue(
    'secondaryBackground.200',
    'secondaryBackground.200'
  )
  const disabledBackground = useColorModeValue('#8799DA', '#6676B7')
  const hoverBackground = useColorModeValue('secondary.500', 'primary.350')
  const filledTrackBackground = useColorModeValue(
    'primaryGlow',
    'primaryIcon.500'
  )

  const [sliderVal, setSliderVal] = useState<number>(sliderValue.value)
  const [showTooltip, setShowTooltip] = useState<boolean>(false)

  const handleSliderChange = (value: number) => {
    setSliderVal(value)
  }

  const handleSliderMouseEnter = () => {
    setShowTooltip(true)
  }

  const handleSliderMouseLeave = () => {
    setShowTooltip(false)
  }

  return (
    <>
      <Slider
        isDisabled={isDisabled}
        aria-label="slider-ex-1"
        defaultValue={sliderVal}
        min={min}
        max={max}
        onChange={handleSliderChange}
        onMouseEnter={handleSliderMouseEnter}
        onMouseLeave={handleSliderMouseLeave}
      >
        <SliderTrack bg={mainBackground}>
          <SliderFilledTrack
            _disabled={{ bg: disabledBackground }}
            _hover={{ bg: hoverBackground }}
            bg={filledTrackBackground}
          />
        </SliderTrack>
        <Tooltip
          title={sliderValue.title}
          isOpen={showTooltip}
          description={sliderValue.description}
          placement={'bottom'}
        >
          <SliderThumb _dark={{ bg: 'frethanWhite' }} />
        </Tooltip>
      </Slider>
    </>
  )
}
