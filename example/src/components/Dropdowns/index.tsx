/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable react/no-unescaped-entities */
import React, { ReactNode } from "react";
import {
  Select,
  useColorModeValue,
} from "@chakra-ui/react";
import { FormControl } from "../InputFields/FormControl";

interface SelectOptionsType {
  key: string,
  value: string | number
}

type InputGroupSize = "lg" | "md" | "sm";

interface InputFieldProps {
    label?: string;
    isRequired?: boolean;
    description?: string;
    helperText?: string;
    type?: string;
    disableGutter?: boolean;
    disabled?: boolean;
    isInvalid?: boolean;
    size?: InputGroupSize;
    errorMessage?: string;
    placeholder?: string;
    leftIcon?: ReactNode;
    rightIcon?: ReactNode;
    value?: string | number;
    onChange?: (e: any) => void;
  }
  

interface SelectFieldProps extends InputFieldProps {
  options: SelectOptionsType[];
}
// eslint-disable-next-line react/display-name
export const Dropdown = React.forwardRef<HTMLSelectElement, SelectFieldProps>(
  (
    {
      label,
      description,
      helperText,
      disableGutter,
      disabled,
      isInvalid,
      size,
      errorMessage,
      isRequired,
      placeholder,
      options,
      ...props
    },
    ref
  ) => {
    return (
      <FormControl
        label={label}
        description={description}
        helperText={helperText}
        disableGutter={disableGutter}
        isInvalid={isInvalid}
        errorMessage={errorMessage}
        isRequired={isRequired}
    >
        <Select
          ref={ref}
          {...props}
          size={size || "lg"}
          isDisabled={disabled}
          _placeholder={{
            color: isInvalid
              ? useColorModeValue("rgba(225, 62, 62, .5)", "frethanErrorText")
              : useColorModeValue("#6E7DB7", "#94A3D9"),
          }}
          placeholder={placeholder}
        >
          {options.map((val, idx) => (
         <option key={idx} value={val.value}>{val.key}</option>
          ))}
        </Select>
      </FormControl>
    );
  }
);
