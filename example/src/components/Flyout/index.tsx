import React, { FC, useRef, useState } from 'react'
import { Menu, FocusableItem } from '@szhsin/react-menu'
import {
  Button,
  Input,
  InputGroup,
  InputLeftElement,
  useColorMode,
} from '@chakra-ui/react'
import { ChevronDownIcon, SearchIcon } from '@chakra-ui/icons'
import { COLORSCHEME } from '@/constants/options'

// import '../../../styles/globals.css'
import { DEFAULT_INPUT_FIELD_WIDTH } from '@/constants/forms'
interface FlyoutProps {
  colorScheme: COLORSCHEME | string
  buttonName: string
  menuChildren: any[]
  hasSearch: boolean
  onSearch: Function
  searchPlaceholder: string
}

export const FlyoutMenu: FC<FlyoutProps> = ({
  buttonName,
  menuChildren,
  colorScheme,
  hasSearch,
  onSearch,
  searchPlaceholder,
}) => {
  const [search, setSearch] = useState('')
  const searchRef = useRef()
  const { colorMode } = useColorMode()
  return (
    <Menu
      theming={colorMode}
      menuButton={
        <Button colorScheme={colorScheme} rightIcon={<ChevronDownIcon />}>
          {buttonName}
        </Button>
      }
    >
      {hasSearch ?? (
        <FocusableItem key={'search'}>
          {() => (
            <InputGroup
              colorScheme={colorScheme}
              style={{
                borderColor: 'none',
                width: DEFAULT_INPUT_FIELD_WIDTH * 0.8,
              }}
              _dark={{ borderColor: 'rgba(0,0,0,0.15)' }}
            >
              <InputLeftElement>
                <SearchIcon color={'primary'} />
              </InputLeftElement>
              <Input
                colorScheme={colorScheme}
                _dark={{ background: '#1D2851' }}
                ref={() => searchRef}
                onChange={({ target: { value } }) => {
                  setSearch(value)
                }}
                placeholder={searchPlaceholder}
              />
            </InputGroup>
          )}
        </FocusableItem>
      )}
      {menuChildren.map((child, index) => (
        <div key={index}>{child}</div>
      ))}
    </Menu>
  )
}
