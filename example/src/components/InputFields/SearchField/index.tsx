/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable react/no-unescaped-entities */
import { CloseIcon, SearchIcon } from '@/components/icons'
import {
  Input,
  InputLeftElement,
  InputProps,
  InputRightElement,
  useColorModeValue,
} from '@chakra-ui/react'
import React from 'react'
import { InputFieldProps } from '../InputField'
import { FormControl } from '@/components/forms/FormControl'

interface SearchFieldProps extends InputFieldProps {
  isClearable?: boolean
  clearInput?: () => void
  inputProps?: InputProps
}

// eslint-disable-next-line react/display-name
export const SearchField = React.forwardRef<HTMLInputElement, SearchFieldProps>(
  (
    {
      label,
      description,
      helperText,
      type,
      disableGutter,
      disabled,
      isInvalid,
      size,
      errorMessage,
      isRequired,
      placeholder,
      isClearable,
      clearInput,
      inputProps,
    },
    ref
  ) => {
    const iconColor = isInvalid
      ? useColorModeValue('frethanError', 'frethanErrorText')
      : disabled
      ? useColorModeValue('frethanDisabled', 'frethanDisabledDarkMode')
      : useColorModeValue('tertiaryTypography.500', 'primaryTypography.200')

    return (
      <FormControl
        label={label}
        description={description}
        helperText={helperText}
        disableGutter={disableGutter}
        isInvalid={isInvalid}
        errorMessage={errorMessage}
        isRequired={isRequired}
      >
        <InputLeftElement h="100%" pointerEvents="none" color={iconColor}>
          <SearchIcon />
        </InputLeftElement>

        <Input
          ref={ref}
          {...inputProps}
          size={size || 'lg'}
          isDisabled={disabled}
          _placeholder={{
            color: isInvalid
              ? useColorModeValue('rgba(225, 62, 62, .5)', 'frethanErrorText')
              : useColorModeValue('#6E7DB7', '#94A3D9'),
          }}
          placeholder={placeholder}
          type="text"
        />

        {isClearable ? (
          <InputRightElement
            h="100%"
            cursor="pointer"
            onClick={() => clearInput && clearInput()}
            color={iconColor}
          >
            <CloseIcon />
          </InputRightElement>
        ) : (
          ''
        )}
      </FormControl>
    )
  }
)
