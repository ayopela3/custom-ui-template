import { ClipIcon, TimesIcon, UploadIcon } from '@/components/icons'
import { Flex, HStack, Text, Icon, VStack, Box } from '@chakra-ui/react'
import { FC, useState } from 'react'
import { Accept, useDropzone } from 'react-dropzone'
import { useTranslation } from 'react-i18next'

interface FileUploadProps {
  onUpload?: (files: File[]) => void
  placeholder?: string
  disabled: boolean
  supportedFiles?: Accept
  errorMessage?: string
}

const FileUpload: FC<FileUploadProps> = ({
  onUpload,
  placeholder,
  disabled = false,
  supportedFiles,
  errorMessage,
}) => {
  const [selectedFiles, setSelectedFiles] = useState<File[]>([])

  const onDropAccepted = (files: File[]) => {
    if (files && files.length) {
      const newSelectedFiles = [...selectedFiles, ...files]
      if (onUpload) {
        onUpload(newSelectedFiles)
      }
      setSelectedFiles(newSelectedFiles)
    }
  }

  const onRemoveFile = (file: File) => {
    if (selectedFiles && selectedFiles.length) {
      const newFiles = selectedFiles.filter((savedFile) => savedFile !== file)
      if (onUpload) {
        onUpload(newFiles)
      }
      setSelectedFiles(newFiles)
    }
  }

  const accept = supportedFiles || {
    'application/pdf': ['.pdf'],
  }

  const { t } = useTranslation('common')

  const { getRootProps, getInputProps, open } = useDropzone({
    disabled,
    noClick: true,
    accept,
    onDropAccepted,
  })

  return (
    <>
      {selectedFiles.length &&
        selectedFiles.map((details, index) => {
          return (
            <VStack
              key={index}
              justifyContent="flex-start"
              alignItems="flex-start"
            >
              <HStack>
                <Text textAlign="left">
                  {details.name} - {details.size}
                </Text>
                <Icon
                  as={TimesIcon}
                  color="red"
                  _hover={{
                    cursor: 'pointer',
                  }}
                  onClick={() => onRemoveFile(details)}
                />
              </HStack>
            </VStack>
          )
        })}
      <Box mt={selectedFiles.length ? '20px' : '0'}>
        <Text
          fontSize="14px"
          fontWeight="500"
          lineHeight="16px"
          color="primaryTypography.500"
        >
          {t('form.upload_file')} &nbsp;
          <Text as="span" color="frethanLightSteelBlue">
            ({t('form.maxsize')})
          </Text>
        </Text>
        <HStack {...getRootProps({ className: 'dropzone' })} mt="8px" w="100%">
          <Flex
            border="2px dashed"
            borderColor="frethanLightSteelBlue"
            p="10.83px"
            borderRadius="8px"
            alignItems="center"
            _hover={{
              cursor: 'pointer',
            }}
            w="100%"
            onClick={open}
          >
            <Icon as={UploadIcon} color="primaryGlow" />
            <Text color="tertiaryTypography.500" pl="10px">
              {placeholder}
            </Text>
            <Icon as={ClipIcon} color="primaryGlow" ml="auto" />
          </Flex>
          <input style={{ display: 'none' }} {...getInputProps()} />
        </HStack>
        {errorMessage && !selectedFiles.length && (
          <Text
            as="span"
            color="red"
            fontSize="12px"
            fontWeight="500"
            lineHeight="16px"
            w="100%"
          >
            {errorMessage}
          </Text>
        )}
      </Box>
    </>
  )
}

export default FileUpload
