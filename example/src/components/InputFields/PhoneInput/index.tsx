/* eslint-disable react-hooks/rules-of-hooks */
import React, { FC, useEffect, useState } from 'react'
import { CountryType, InputGroupSizes } from '@/lib/types'
import { AsYouType } from "libphonenumber-js"
import Flag from 'react-world-flags'
import { Box, Flex, Icon, Input, InputGroup, InputLeftElement, InputProps, Select, useColorModeValue } from '@chakra-ui/react'
import { count } from 'console'

interface PhoneInputProps {
    value: any
    country: CountryType
    onChange: any
    options: any
    disabled?: boolean
    size?: InputGroupSizes 
    errorMessage?: string;
    isInvalid?: boolean
    inputGroupProps: InputProps
}

export const PhoneInput: FC<PhoneInputProps> = ({value, country, onChange, options, disabled, isInvalid, size, inputGroupProps }) => {
    let [number, setNumber] = useState(value || "")
    let [selectedCountry, setSelectedCountry] = useState(country)
    let [countryCode, setCountryCode] = useState(country.phone || '')

    const getCountryTelCode = (country: CountryType) => {
        try {
          let x = country && options.find((c: any) => c.label === country).phone      
          return `${x}`         
        } catch (error) {
            console.log(error)
            return ''
        }
    }

    const onCountryChange = (e: any) => {
        let value = e.target.value;
        let x = value && options.find((c: any) => c.label === value)   
        let code = getCountryTelCode(value);
        let parsedNumber = new AsYouType().input(`${code}${number}`);
        setCountryCode(code);
        setSelectedCountry(x);
        return onChange({code, number, parsedNumber });
      };
    
    const onPhoneNumberChange = (e: any) => {
        let value = e.target.value;
        let parsedNumber = new AsYouType().input(`${countryCode}${value}`);
        setNumber(value);
        return onChange({code: countryCode, number, parsedNumber});
    };

    return (
        <InputGroup {...inputGroupProps}>
      <InputLeftElement width="4em">
        <Select
          top="3"
          left="3"
          zIndex={1}
          bottom={0}
          opacity={0}
          height="100%"
          position="absolute"
          value={selectedCountry as any}
          onChange={onCountryChange}
        >
          <option />
          {options.map((option: {value: any, label: string}, index: any) => (
            <option key={index} value={option.value}>{option.label}</option>
          ))}
        </Select>
        {selectedCountry ? (
            <Box mr="12px" mt={'1'} width="32px" >
              <Flag style={{borderRadius: 4}} height="1.2rem" code={selectedCountry.code} />
            </Box>
          ) : (
            null
          )}
      </InputLeftElement>
      <Input
       
        padding="16px 42px"
        pl="4.4rem"
        type="tel"
        value={number}
        pattern="[0-9]"
        placeholder={''}
        size={size || "lg"}
        isDisabled={disabled}
        _placeholder={{
          color: isInvalid
            ? useColorModeValue("rgba(225, 62, 62, .5)", "frethanErrorText")
            : useColorModeValue("#6E7DB7", "#94A3D9"),
        }}
        onChange={onPhoneNumberChange}
      />
    </InputGroup>
    )
}

PhoneInput.defaultProps = {
    options: []
}