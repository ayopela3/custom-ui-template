import React, { FC } from "react";
import {
  Textarea,
  Box,
  useColorMode,
  Text
} from "@chakra-ui/react";
import { SIZE } from '@/constants/options';
import { InputFieldVariant } from '@/lib/types'

interface TextAreaProp {
  label?: string;
  description?: string;
  helperText?: string;
  isOptional?: boolean
  isRequired?: boolean
  placeholder?: string;
  backgroundColor?: string;
  textColor?: string;
  secondaryTextColor?: string;
  variant?: InputFieldVariant
}

export const TextArea: FC<TextAreaProp> = ({
  label,
  description,
  helperText,
  placeholder,
  isOptional,
  isRequired,
  backgroundColor,
  textColor,
  secondaryTextColor,
  variant,
}) => {

  const { colorMode } = useColorMode();

  secondaryTextColor = colorMode === 'dark' ? 'white' : secondaryTextColor;
  return (
    <Box
      style={{
        backgroundColor: backgroundColor,
        width: '310px',
        color: textColor,
        textAlign: 'left'
      }}
    >
      <label
        style={{
          fontSize: "14px"
        }}
      >
        <Text variant="labelMed" color="blackAlpha" mb="4px">
          {label} {!!isOptional && <Text as="span" color="frethanGrey">(optional)</Text>}
        </Text>
        {isRequired && <span style={{ float: "right", color: "#9D0D11" }}>*</span>}
        <Text variant="captionMed" color="frethanDarkGrey">
          {description}
        </Text>
        {/* <p style={{ fontSize: "12px", color: textColor, paddingBottom: "5px" }}>{description}</p> */}
        <Textarea
          variant={variant}
          width={310}
          placeholder={placeholder}
          style={{

          }}
        >
        </Textarea>

      </label>
      <p><span style={{ color: secondaryTextColor, fontSize: "12px" }}>{helperText}</span></p>
    </Box>
  );
};

TextArea.defaultProps = {
  label: "",
  description: "",
  placeholder: "",
  helperText: "",
  secondaryTextColor: "#898A94",
  isRequired: false
};
