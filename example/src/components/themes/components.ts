import {
  StyleFunctionProps,
  createMultiStyleConfigHelpers,
} from "@chakra-ui/react";
import { switchAnatomy } from "@chakra-ui/anatomy";

const { definePartsStyle, defineMultiStyleConfig } =
  createMultiStyleConfigHelpers(switchAnatomy.keys);

const Heading = {
  variants: {
    statesHeader: {
      fontWeight: 500,
      fontSize: "24px",
      lineHeight: "32px",
    },
  },
};

const Text = {
  variants: {
    labelMed: {
      fontSize: "14px",
      lineHeight: "16px",
      fontWeight: "500",
    },
    captionMed: {
      fontSize: "12px",
      lineHeight: "14px",
      fontWeight: "500",
    },
    statesDescription: {
      fontSize: "16px",
      lineHeight: "20px",
      fontWeight: "400",
    },
  },
};

const Link = {
  variants: {
    hyperlinkNormal: {
      fontWeight: 500,
      textDecoration: "underline",
      color: "primary.400",
    },
    hyperlinkSolid: {
      fontWeight: 500,
      textDecoration: "underline",
      color: "primary.450",
    },
  },
};

const Select = {
  variants: {
    outline: (props: StyleFunctionProps) => ({
      field: {
        fontSize: "0.875rem",
        borderRadius: ".5rem",
        fontWeight: 500,
        bg:
          props.colorMode !== "dark" ? "frethanWhite" : "neutralBackground.200",
        borderColor:
          props.colorMode !== "dark" ? "#B0BBE5" : "secondaryTypography.500",
        _hover: {
          borderColor:
            props.colorMode !== "dark" ? "secondary.500" : "primary.350",
        },
        _focusVisible: {
          boxShadow:
            props.colorMode !== "dark"
              ? "0px 0px 5px 4px rgb(58 125 246 / 41%)"
              : "0px 0px 3px 4px rgb(58 125 246 / 77%)",
          borderColor:
            props.colorMode !== "dark" ? "frethanSoftBlue" : "primary.350",
        },
        _invalid: {
          bw: 1,
          bg:
            props.colorMode !== "dark" ? "frethanMistyRose" : "frethanDarkRed",
          borderColor:
            props.colorMode !== "dark" ? "errorGlow" : "errorStroke.200",
          boxShadow: "none",
        },
        _disabled: {
          opacity: 1,
          color:
            props.colorMode !== "dark"
              ? "frethanDisabled"
              : "frethanDisabledDarkMode",
          bg:
            props.colorMode !== "dark"
              ? "secondaryStroke.500"
              : "frethanAmericanBlue",
          borderColor:
            props.colorMode !== "dark"
              ? "secondaryStroke.500"
              : "frethanAmericanBlue",
          _hover: {
            borderColor:
              props.colorMode !== "dark"
                ? "secondaryStroke.500"
                : "frethanAmericanBlue",
          },
        },
      },
    }),
    default: (props: StyleFunctionProps) => ({
      // we will be passing the props to customize its dark mode variant
      _placeholder: {
        // color: 'frethanLightGrey'
      },
      field: {
        _invalid: {
          _placeholder: {
            color: "frethanBlack",
          },
          color: "frethanBlack",
          backgroundColor:
            props.colorMode !== "dark" ? "frethanMistyRose" : "none",
          border: "1px solid",
          borderColor: "frethanCavernPink",
        },
        _disabled: {
          _placeholder: {
            color: "frethanGrey",
          },
          color: "frethanGrey",
          backgroundColor:
            props.colorMode !== "dark" ? "frethanMercury" : "none",
          border: "1px solid",
          borderColor: "frethanMercury",
        },
        _active: {
          _placeholder: {
            color: "frethanLightGrey",
          },
          color: "hcDarkGrey",
          border: "1px solid",
          borderColor: "frethanSoftBlue",
        },
        _focus: {
          _placeholder: {
            color: "frethanLightGrey",
          },
          color: "hcDarkGrey",
          boxShadow: "0 0 1px 3px rgba(196, 216, 252, 1)",
          border: "1px solid",
          borderColor: "rgba(78, 138, 247, 1)",
        },
        color: "hcDarkGrey",
        backgroundColor: "none",
        border: "1px solid",
        borderColor: "primaryStroke.500",
      },
    }),
    solidBlueSecondary: (props: StyleFunctionProps) => ({
      field: {
        _placeholder: {
          color: "frethanSoftBlue",
        },
        _focus: {
          _placeholder: {
            color: "frethanSoftBlue",
          },
          color: "hcDarkGrey",
          boxShadow: "0 0 1px 3px rgba(196, 216, 252, 1)",
          border: "1px solid",
          borderColor: "rgba(78, 138, 247, 1)",
        },
        color: "hcDarkGrey",
        border: "1px solid",
        borderColor: "frethanSoftBlue",
      },
    }),
    defaultPlainText: (props: StyleFunctionProps) => ({
      _placeholder: {
        // color: 'frethanLightGrey'
      },
      field: {
        _invalid: {
          backgroundColor:
            props.colorMode === "light" ? "frethanMistyRose" : "frethanDarkRed",
          border: "1px solid",
          borderColor:
            props.colorMode === "light"
              ? "frethanCavernPink"
              : "errorStroke.200",
        },
        _disabled: {
          backgroundColor:
            props.colorMode === "light"
              ? "frethanMercury"
              : "frethanAmericanBlue",
          border: "1px solid",
          borderColor: "frethanMercury",
        },
        _active: {
          backgroundColor:
            props.colorMode === "light"
              ? "frethanWhite"
              : "secondaryTypography.500",
          border: "1px solid",
          borderColor: "frethanSoftBlue",
        },
        _focus: {
          backgroundColor:
            props.colorMode === "light"
              ? "frethanWhite"
              : "secondaryTypography.500",
          boxShadow:
            props.colorMode === "light"
              ? "0 0 1px 3px rgba(196, 216, 252, 1)"
              : "0 0 1px 3px rgba(78, 138, 247, 1)",
          border: "1px solid",
          borderColor: "rgba(78, 138, 247, 1)",
        },
        backgroundColor:
          props.colorMode === "light"
            ? "frethanWhite"
            : "secondaryTypography.500",
        border: "1px solid",
        borderColor:
          props.colorMode === "light"
            ? "primaryStroke.500"
            : "secondaryTypography.500",
      },
    }),
    solidBluePlainText: (props: StyleFunctionProps) => ({
      _placeholder: {
        color: "frethanLightGrey",
      },
      field: {
        _focus: {
          backgroundColor:
            props.colorMode === "light"
              ? "frethanWhite"
              : "secondaryTypography.500",
          boxShadow:
            props.colorMode === "light"
              ? "0 0 1px 3px rgba(196, 216, 252, 1)"
              : "0 0 1px 3px rgba(78, 138, 247, 1)",
          border: "1px solid",
          borderColor: "rgba(78, 138, 247, 1)",
        },
        backgroundColor:
          props.colorMode === "light"
            ? "frethanWhite"
            : "secondaryTypography.500",
        border: "1px solid",
        borderColor:
          props.colorMode === "light" ? "frethanSoftBlue" : "primary.350",
      },
    }),
    solidGreyPlainText: (props: StyleFunctionProps) => ({
      _placeholder: {
        color: "frethanBlack",
      },
      field: {
        _focus: {
          backgroundColor:
            props.colorMode === "light"
              ? "frethanWhite"
              : "secondaryTypography.500",
          boxShadow:
            props.colorMode === "light"
              ? "0 0 1px 3px rgba(196, 216, 252, 1)"
              : "0 0 1px 3px rgba(78, 138, 247, 1)",
          border: "1px solid",
          borderColor: "rgba(78, 138, 247, 1)",
        },
        backgroundColor:
          props.colorMode === "light"
            ? "frethanWhite"
            : "secondaryTypography.500",
        border: "1px solid",
        borderColor:
          props.colorMode === "light"
            ? "frethanSolidGrey"
            : "secondaryTypography.500",
      },
    }),
    plain: (props: StyleFunctionProps) => ({
      // we will be passing the props to customize its dark mode variant
      _placeholder: {
        // color: 'frethanLightGrey'
      },
      field: {
        color: "hcDarkGrey",
        backgroundColor: "none",
        border: "1px solid",
        borderColor: "primaryStroke.500",
      },
    }),
    solidBlue: (props: StyleFunctionProps) => ({
      _placeholder: {
        color: "frethanLightGrey",
      },
      field: {
        _focus: {
          _placeholder: {
            color: "frethanLightGrey",
          },
          color: "hcDarkGrey",
          boxShadow: "0 0 1px 3px rgba(196, 216, 252, 1)",
          border: "1px solid",
          borderColor: "rgba(78, 138, 247, 1)",
        },
        color: "hcDarkGrey",
        border: "1px solid",
        borderColor: "frethanSoftBlue",
      },
    }),
    glowBlue: (props: StyleFunctionProps) => ({
      _placeholder: {
        color: "frethanLightGrey",
      },
      field: {
        color: "hcDarkGrey",
        boxShadow: "0 0 1px 3px rgba(196, 216, 252, 1)",
        border: "1px solid",
        borderColor: "rgba(78, 138, 247, 1)",
      },
    }),
    solidGrey: (props: StyleFunctionProps) => ({
      _placeholder: {
        color: "frethanBlack",
      },
      field: {
        _focus: {
          _placeholder: {
            color: "frethanLightGrey",
          },
          color: "hcDarkGrey",
          boxShadow: "0 0 1px 3px rgba(196, 216, 252, 1)",
          border: "1px solid",
          borderColor: "rgba(78, 138, 247, 1)",
        },
        color: "frethanBlack",
        backgroundColor: props.colorMode !== "dark" ? "frethanWhite" : "none",
        border: "1px solid",
        borderColor: "frethanSolidGrey",
      },
    }),
    dangerField: (props: StyleFunctionProps) => ({
      _placeholder: {
        color: "frethanBlack",
      },
      field: {
        color: "frethanBlack",
        backgroundColor:
          props.colorMode !== "dark" ? "frethanMistyRose" : "none",
        border: "1px solid",
        borderColor: "frethanCavernPink",
      },
    }),
    disabledField: (props: StyleFunctionProps) => ({
      _placeholder: {
        color: "frethanGrey",
      },
      field: {
        color: "frethanGrey",
        backgroundColor: props.colorMode !== "dark" ? "frethanMercury" : "none",
        border: "1px solid",
        borderColor: "frethanMercury",
      },
    }),
    successField: (props: StyleFunctionProps) => ({
      _placeholder: {
        color: "frethanBlack",
      },
      field: {
        _focus: {
          _placeholder: {
            color: "frethanLightGrey",
          },
          color: "hcDarkGrey",
          boxShadow: "0 0 1px 3px rgba(196, 216, 252, 1)",
          border: "1px solid",
          borderColor: "rgba(78, 138, 247, 1)",
        },
        color: "frethanBlack",
        backgroundColor: props.colorMode !== "dark" ? "frethanWhite" : "none",
        border: "1px solid",
        borderColor: "frethanPaleAqua",
      },
    }),
  },
};

const Textarea = {
  variants: {
    outline: (props: StyleFunctionProps) => ({
      field: {
        fontSize: "0.875rem",
        borderRadius: ".5rem",
        fontWeight: 500,
        bg:
          props.colorMode !== "dark" ? "frethanWhite" : "neutralBackground.200",
        borderColor:
          props.colorMode !== "dark" ? "#B0BBE5" : "secondaryTypography.500",
        _hover: {
          borderColor:
            props.colorMode !== "dark" ? "secondary.500" : "primary.350",
        },
        _focusVisible: {
          boxShadow:
            props.colorMode !== "dark"
              ? "0px 0px 5px 4px rgb(58 125 246 / 41%)"
              : "0px 0px 3px 4px rgb(58 125 246 / 77%)",
          borderColor:
            props.colorMode !== "dark" ? "frethanSoftBlue" : "primary.350",
        },
        _invalid: {
          bw: 1,
          bg:
            props.colorMode !== "dark" ? "frethanMistyRose" : "frethanDarkRed",
          borderColor:
            props.colorMode !== "dark" ? "errorGlow" : "errorStroke.200",
          boxShadow: "none",
        },
        _disabled: {
          opacity: 1,
          color:
            props.colorMode !== "dark"
              ? "frethanDisabled"
              : "frethanDisabledDarkMode",
          bg:
            props.colorMode !== "dark"
              ? "secondaryStroke.500"
              : "frethanAmericanBlue",
          borderColor:
            props.colorMode !== "dark"
              ? "secondaryStroke.500"
              : "frethanAmericanBlue",
          _hover: {
            borderColor:
              props.colorMode !== "dark"
                ? "secondaryStroke.500"
                : "frethanAmericanBlue",
          },
        },
      },
    }),
    default: (props: StyleFunctionProps) => ({
      // we will be passing the props to customize its dark mode variant
      _placeholder: {
        // color: 'frethanLightGrey'
      },
      field: {
        _invalid: {
          _placeholder: {
            color: "frethanBlack",
          },
          color: "frethanBlack",
          backgroundColor:
            props.colorMode !== "dark" ? "frethanMistyRose" : "none",
          border: "1px solid",
          borderColor: "frethanCavernPink",
        },
        _disabled: {
          _placeholder: {
            color: "frethanGrey",
          },
          color: "frethanGrey",
          backgroundColor:
            props.colorMode !== "dark" ? "frethanMercury" : "none",
          border: "1px solid",
          borderColor: "frethanMercury",
        },
        _active: {
          _placeholder: {
            color: "frethanLightGrey",
          },
          color: "hcDarkGrey",
          border: "1px solid",
          borderColor: "frethanSoftBlue",
        },
        _focus: {
          _placeholder: {
            color: "frethanLightGrey",
          },
          color: "hcDarkGrey",
          boxShadow: "0 0 1px 3px rgba(196, 216, 252, 1)",
          border: "1px solid",
          borderColor: "rgba(78, 138, 247, 1)",
        },
        color: "hcDarkGrey",
        backgroundColor: "none",
        border: "1px solid",
        borderColor: "primaryStroke.500",
      },
    }),
    defaultPlainText: (props: StyleFunctionProps) => ({
      _placeholder: {
        // color: 'frethanLightGrey'
      },
      field: {
        _invalid: {
          backgroundColor:
            props.colorMode === "light" ? "frethanMistyRose" : "frethanDarkRed",
          border: "1px solid",
          borderColor:
            props.colorMode === "light"
              ? "frethanCavernPink"
              : "errorStroke.200",
        },
        _disabled: {
          backgroundColor:
            props.colorMode === "light"
              ? "frethanMercury"
              : "frethanAmericanBlue",
          border: "1px solid",
          borderColor: "frethanMercury",
        },
        _active: {
          backgroundColor:
            props.colorMode === "light"
              ? "frethanWhite"
              : "secondaryTypography.500",
          border: "1px solid",
          borderColor: "frethanSoftBlue",
        },
        _focus: {
          backgroundColor:
            props.colorMode === "light"
              ? "frethanWhite"
              : "secondaryTypography.500",
          boxShadow:
            props.colorMode === "light"
              ? "0 0 1px 3px rgba(196, 216, 252, 1)"
              : "0 0 1px 3px rgba(78, 138, 247, 1)",
          border: "1px solid",
          borderColor: "rgba(78, 138, 247, 1)",
        },
        backgroundColor:
          props.colorMode === "light"
            ? "frethanWhite"
            : "secondaryTypography.500",
        border: "1px solid",
        borderColor:
          props.colorMode === "light"
            ? "primaryStroke.500"
            : "secondaryTypography.500",
      },
    }),
    solidBluePlainText: (props: StyleFunctionProps) => ({
      _placeholder: {
        color: "frethanLightGrey",
      },
      field: {
        _focus: {
          backgroundColor:
            props.colorMode === "light"
              ? "frethanWhite"
              : "secondaryTypography.500",
          boxShadow:
            props.colorMode === "light"
              ? "0 0 1px 3px rgba(196, 216, 252, 1)"
              : "0 0 1px 3px rgba(78, 138, 247, 1)",
          border: "1px solid",
          borderColor: "rgba(78, 138, 247, 1)",
        },
        backgroundColor:
          props.colorMode === "light"
            ? "frethanWhite"
            : "secondaryTypography.500",
        border: "1px solid",
        borderColor:
          props.colorMode === "light" ? "frethanSoftBlue" : "primary.350",
      },
    }),
    solidGreyPlainText: (props: StyleFunctionProps) => ({
      _placeholder: {
        color: "frethanBlack",
      },
      field: {
        _focus: {
          backgroundColor:
            props.colorMode === "light"
              ? "frethanWhite"
              : "secondaryTypography.500",
          boxShadow:
            props.colorMode === "light"
              ? "0 0 1px 3px rgba(196, 216, 252, 1)"
              : "0 0 1px 3px rgba(78, 138, 247, 1)",
          border: "1px solid",
          borderColor: "rgba(78, 138, 247, 1)",
        },
        backgroundColor:
          props.colorMode === "light"
            ? "frethanWhite"
            : "secondaryTypography.500",
        border: "1px solid",
        borderColor:
          props.colorMode === "light"
            ? "frethanSolidGrey"
            : "secondaryTypography.500",
      },
    }),
    plain: (props: StyleFunctionProps) => ({
      // we will be passing the props to customize its dark mode variant
      _placeholder: {
        // color: 'frethanLightGrey'
      },
      field: {
        _focus: {
          backgroundColor:
            props.colorMode === "light"
              ? "frethanWhite"
              : "secondaryTypography.500",
          boxShadow:
            props.colorMode === "light"
              ? "0 0 1px 3px rgba(196, 216, 252, 1)"
              : "0 0 1px 3px rgba(78, 138, 247, 1)",
          border: "1px solid",
          borderColor: "rgba(78, 138, 247, 1)",
        },
        color: "hcDarkGrey",
        backgroundColor: "none",
        border: "1px solid",
        borderColor: "primaryStroke.500",
      },
    }),
    solidBlue: (props: StyleFunctionProps) => ({
      _placeholder: {
        color: "frethanLightGrey",
      },
      field: {
        _focus: {
          _placeholder: {
            color: "frethanLightGrey",
          },
          color: "hcDarkGrey",
          boxShadow: "0 0 1px 3px rgba(196, 216, 252, 1)",
          border: "1px solid",
          borderColor: "rgba(78, 138, 247, 1)",
        },
        color: "hcDarkGrey",
        border: "1px solid",
        borderColor: "frethanSoftBlue",
      },
    }),
    solidBlueSecondary: (props: StyleFunctionProps) => ({
      border: "1px solid",
      borderColor: "frethanSoftBlue",
      color: "frethanSoftBlue",
      _placeholder: {
        color: "frethanSoftBlue",
      },
      field: {
        _placeholder: {
          color: "frethanSoftBlue",
        },
        _focus: {
          _placeholder: {
            color: "frethanSoftBlue",
          },
          color: "frethanSoftBlue",
          boxShadow: "0 0 1px 3px rgba(196, 216, 252, 1)",
          border: "1px solid",
          borderColor: "rgba(78, 138, 247, 1)",
        },
        color: "frethanSoftBlue",
        border: "1px solid",
        borderColor: "frethanSoftBlue",
      },
    }),
    glowBlue: (props: StyleFunctionProps) => ({
      _placeholder: {
        color: "frethanLightGrey",
      },
      field: {
        color: "hcDarkGrey",
        boxShadow: "0 0 1px 3px rgba(196, 216, 252, 1)",
        border: "1px solid",
        borderColor: "rgba(78, 138, 247, 1)",
      },
    }),
    solidGrey: (props: StyleFunctionProps) => ({
      _placeholder: {
        color: "frethanBlack",
      },
      field: {
        _focus: {
          _placeholder: {
            color: "frethanLightGrey",
          },
          color: "hcDarkGrey",
          boxShadow: "0 0 1px 3px rgba(196, 216, 252, 1)",
          border: "1px solid",
          borderColor: "rgba(78, 138, 247, 1)",
        },
        color: "frethanBlack",
        backgroundColor: props.colorMode !== "dark" ? "frethanWhite" : "none",
        border: "1px solid",
        borderColor: "frethanSolidGrey",
      },
    }),
    dangerField: (props: StyleFunctionProps) => ({
      _placeholder: {
        color: "frethanBlack",
      },
      color: "frethanBlack",
      backgroundColor:
        props.colorMode !== "dark" ? "frethanMistyRose" : "none",
      border: "1px solid",
      borderColor: "frethanCavernPink",
      field: {
        color: "frethanBlack",
        backgroundColor:
          props.colorMode !== "dark" ? "frethanMistyRose" : "none",
        border: "1px solid",
        borderColor: "frethanCavernPink",
      },
    }),
    disabledField: (props: StyleFunctionProps) => ({
      _placeholder: {
        color: "frethanGrey",
      },
      field: {
        color: "frethanGrey",
        backgroundColor: props.colorMode !== "dark" ? "frethanMercury" : "none",
        border: "1px solid",
        borderColor: "frethanMercury",
      },
    }),
    successField: (props: StyleFunctionProps) => ({
      _placeholder: {
        color: "frethanBlack",
      },
      field: {
        _focus: {
          _placeholder: {
            color: "frethanLightGrey",
          },
          color: "hcDarkGrey",
          boxShadow: "0 0 1px 3px rgba(196, 216, 252, 1)",
          border: "1px solid",
          borderColor: "rgba(78, 138, 247, 1)",
        },
        color: "frethanBlack",
        backgroundColor: props.colorMode !== "dark" ? "frethanWhite" : "none",
        border: "1px solid",
        borderColor: "frethanPaleAqua",
      },
    }),
  },
};

const Input = {
  variants: {
    outline: (props: StyleFunctionProps) => ({
      field: {
        fontSize: "0.875rem",
        borderRadius: ".5rem",
        fontWeight: 500,
        bg:
          props.colorMode !== "dark" ? "frethanWhite" : "neutralBackground.200",
        borderColor:
          props.colorMode !== "dark" ? "#B0BBE5" : "secondaryTypography.500",
        _hover: {
          borderColor:
            props.colorMode !== "dark" ? "secondary.500" : "primary.350",
        },
        _focusVisible: {
          boxShadow:
            props.colorMode !== "dark"
              ? "0px 0px 5px 4px rgb(58 125 246 / 41%)"
              : "0px 0px 3px 4px rgb(58 125 246 / 77%)",
          borderColor:
            props.colorMode !== "dark" ? "frethanSoftBlue" : "primary.350",
        },
        _invalid: {
          bw: 1,
          bg:
            props.colorMode !== "dark" ? "frethanMistyRose" : "frethanDarkRed",
          borderColor:
            props.colorMode !== "dark" ? "errorGlow" : "errorStroke.200",
          boxShadow: "none",
        },
        _disabled: {
          opacity: 1,
          color:
            props.colorMode !== "dark"
              ? "frethanDisabled"
              : "frethanDisabledDarkMode",
          bg:
            props.colorMode !== "dark"
              ? "secondaryStroke.500"
              : "frethanAmericanBlue",
          borderColor:
            props.colorMode !== "dark"
              ? "secondaryStroke.500"
              : "frethanAmericanBlue",
          _hover: {
            borderColor:
              props.colorMode !== "dark"
                ? "secondaryStroke.500"
                : "frethanAmericanBlue",
          },
        },
      },
    }),
    default: (props: StyleFunctionProps) => ({
      // we will be passing the props to customize its dark mode variant
      _placeholder: {
        // color: 'frethanLightGrey'
      },
      field: {
        _invalid: {
          _placeholder: {
            color: "frethanBlack",
          },
          color: "frethanBlack",
          backgroundColor:
            props.colorMode !== "dark" ? "frethanMistyRose" : "none",
          border: "1px solid",
          borderColor: "frethanCavernPink",
        },
        _disabled: {
          _placeholder: {
            color: "frethanGrey",
          },
          color: "frethanGrey",
          backgroundColor:
            props.colorMode !== "dark" ? "frethanMercury" : "none",
          border: "1px solid",
          borderColor: "frethanMercury",
        },
        _active: {
          _placeholder: {
            color: "frethanLightGrey",
          },
          color: "hcDarkGrey",
          border: "1px solid",
          borderColor: "frethanSoftBlue",
        },
        _focus: {
          _placeholder: {
            color: "frethanLightGrey",
          },
          color: "hcDarkGrey",
          boxShadow: "0 0 1px 3px rgba(196, 216, 252, 1)",
          border: "1px solid",
          borderColor: "rgba(78, 138, 247, 1)",
        },
        color: "hcDarkGrey",
        backgroundColor: "none",
        border: "1px solid",
        borderColor: "primaryStroke.500",
      },
    }),
    defaultPlainText: (props: StyleFunctionProps) => ({
      _placeholder: {
        // color: 'frethanLightGrey'
      },
      field: {
        _invalid: {
          backgroundColor:
            props.colorMode === "light" ? "frethanMistyRose" : "frethanDarkRed",
          border: "1px solid",
          borderColor:
            props.colorMode === "light"
              ? "frethanCavernPink"
              : "errorStroke.200",
        },
        _disabled: {
          backgroundColor:
            props.colorMode === "light"
              ? "frethanMercury"
              : "frethanAmericanBlue",
          border: "1px solid",
          borderColor: "frethanMercury",
        },
        _active: {
          backgroundColor:
            props.colorMode === "light"
              ? "frethanWhite"
              : "secondaryTypography.500",
          border: "1px solid",
          borderColor: "frethanSoftBlue",
        },
        _focus: {
          backgroundColor:
            props.colorMode === "light"
              ? "frethanWhite"
              : "secondaryTypography.500",
          boxShadow:
            props.colorMode === "light"
              ? "0 0 1px 3px rgba(196, 216, 252, 1)"
              : "0 0 1px 3px rgba(78, 138, 247, 1)",
          border: "1px solid",
          borderColor: "rgba(78, 138, 247, 1)",
        },
        backgroundColor:
          props.colorMode === "light"
            ? "frethanWhite"
            : "secondaryTypography.500",
        border: "1px solid",
        borderColor:
          props.colorMode === "light"
            ? "primaryStroke.500"
            : "secondaryTypography.500",
      },
    }),
    solidBluePlainText: (props: StyleFunctionProps) => ({
      _placeholder: {
        color: "frethanLightGrey",
      },
      field: {
        _focus: {
          backgroundColor:
            props.colorMode === "light"
              ? "frethanWhite"
              : "secondaryTypography.500",
          boxShadow:
            props.colorMode === "light"
              ? "0 0 1px 3px rgba(196, 216, 252, 1)"
              : "0 0 1px 3px rgba(78, 138, 247, 1)",
          border: "1px solid",
          borderColor: "rgba(78, 138, 247, 1)",
        },
        backgroundColor:
          props.colorMode === "light"
            ? "frethanWhite"
            : "secondaryTypography.500",
        border: "1px solid",
        borderColor:
          props.colorMode === "light" ? "frethanSoftBlue" : "primary.350",
      },
    }),
    solidGreyPlainText: (props: StyleFunctionProps) => ({
      _placeholder: {
        color: "frethanBlack",
      },
      field: {
        _focus: {
          backgroundColor:
            props.colorMode === "light"
              ? "frethanWhite"
              : "secondaryTypography.500",
          boxShadow:
            props.colorMode === "light"
              ? "0 0 1px 3px rgba(196, 216, 252, 1)"
              : "0 0 1px 3px rgba(78, 138, 247, 1)",
          border: "1px solid",
          borderColor: "rgba(78, 138, 247, 1)",
        },
        backgroundColor:
          props.colorMode === "light"
            ? "frethanWhite"
            : "secondaryTypography.500",
        border: "1px solid",
        borderColor:
          props.colorMode === "light"
            ? "frethanSolidGrey"
            : "secondaryTypography.500",
      },
    }),
    plain: (props: StyleFunctionProps) => ({
      // we will be passing the props to customize its dark mode variant
      _placeholder: {
        // color: 'frethanLightGrey'
      },
      field: {
        _focus: {
          backgroundColor:
            props.colorMode === "light"
              ? "frethanWhite"
              : "secondaryTypography.500",
          boxShadow:
            props.colorMode === "light"
              ? "0 0 1px 3px rgba(196, 216, 252, 1)"
              : "0 0 1px 3px rgba(78, 138, 247, 1)",
          border: "1px solid",
          borderColor: "rgba(78, 138, 247, 1)",
        },
        color: "hcDarkGrey",
        backgroundColor: "none",
        border: "1px solid",
        borderColor: "primaryStroke.500",
      },
    }),
    solidBlue: (props: StyleFunctionProps) => ({
      _placeholder: {
        color: "frethanLightGrey",
      },
      field: {
        _focus: {
          _placeholder: {
            color: "frethanLightGrey",
          },
          color: "hcDarkGrey",
          boxShadow: "0 0 1px 3px rgba(196, 216, 252, 1)",
          border: "1px solid",
          borderColor: "rgba(78, 138, 247, 1)",
        },
        color: "hcDarkGrey",
        border: "1px solid",
        borderColor: "frethanSoftBlue",
      },
    }),
    solidBlueSecondary: (props: StyleFunctionProps) => ({
      field: {
        _placeholder: {
          color: "frethanSoftBlue",
        },
        _focus: {
          _placeholder: {
            color: "frethanSoftBlue",
          },
          color: "frethanSoftBlue",
          boxShadow: "0 0 1px 3px rgba(196, 216, 252, 1)",
          border: "1px solid",
          borderColor: "rgba(78, 138, 247, 1)",
        },
        color: "frethanSoftBlue",
        border: "1px solid",
        borderColor: "frethanSoftBlue",
      },
    }),
    glowBlue: (props: StyleFunctionProps) => ({
      _placeholder: {
        color: "frethanLightGrey",
      },
      field: {
        color: "hcDarkGrey",
        boxShadow: "0 0 1px 3px rgba(196, 216, 252, 1)",
        border: "1px solid",
        borderColor: "rgba(78, 138, 247, 1)",
      },
    }),
    solidGrey: (props: StyleFunctionProps) => ({
      _placeholder: {
        color: "frethanBlack",
      },
      field: {
        _focus: {
          _placeholder: {
            color: "frethanLightGrey",
          },
          color: "hcDarkGrey",
          boxShadow: "0 0 1px 3px rgba(196, 216, 252, 1)",
          border: "1px solid",
          borderColor: "rgba(78, 138, 247, 1)",
        },
        color: "frethanBlack",
        backgroundColor: props.colorMode !== "dark" ? "frethanWhite" : "none",
        border: "1px solid",
        borderColor: "frethanSolidGrey",
      },
    }),
    dangerField: (props: StyleFunctionProps) => ({
      _placeholder: {
        color: "frethanBlack",
      },
      field: {
        color: "frethanBlack",
        backgroundColor:
          props.colorMode !== "dark" ? "frethanMistyRose" : "none",
        border: "1px solid",
        borderColor: "frethanCavernPink",
      },
    }),
    disabledField: (props: StyleFunctionProps) => ({
      _placeholder: {
        color: "frethanGrey",
      },
      field: {
        color: "frethanGrey",
        backgroundColor: props.colorMode !== "dark" ? "frethanMercury" : "none",
        border: "1px solid",
        borderColor: "frethanMercury",
      },
    }),
    successField: (props: StyleFunctionProps) => ({
      _placeholder: {
        color: "frethanBlack",
      },
      field: {
        _focus: {
          _placeholder: {
            color: "frethanLightGrey",
          },
          color: "hcDarkGrey",
          boxShadow: "0 0 1px 3px rgba(196, 216, 252, 1)",
          border: "1px solid",
          borderColor: "rgba(78, 138, 247, 1)",
        },
        color: "frethanBlack",
        backgroundColor: props.colorMode !== "dark" ? "frethanWhite" : "none",
        border: "1px solid",
        borderColor: "frethanPaleAqua",
      },
    }),
  },
};

const Button = {
  variants: {
    custom: (props: StyleFunctionProps) => ({
      padding: "8px 16px",
      background: props.colorMode !== "dark" ? "white" : "black",
      color: props.colorMode !== "dark" ? "#202020" : "white",
    }),
    socialButton: (props: StyleFunctionProps) => ({
      padding: "8px 16px",
      background: props.colorMode !== "dark" ? "#FFF" : "#1D2851",
      border:
        props.colorMode !== "dark"
          ? "1px solid #EAEAEA"
          : "1px solid rgba(0,0,0,0.1)",
      borderRadius: "8px",
      fontSize: "16px",
      fontWeight: "500",
      lineHeight: "20px",
      color: props.colorMode === "dark" ? "#FFFFFF" : "#222222",
    }),
  },
};

const Checkbox = {
  baseStyle: {
    control: {
      borderRadius: "6px",
      _hover: {
        opacity: "0.8",
        _disabled: {
          opacity: "0.4",
        },
      },
      _disabled: {
        opacity: "0.4",
      },
    },
  },
  sizes: {
    lg: {
      control: {
        width: "24px",
        height: "24px",
      },
      label: {
        fontWeight: "500",
        fontSize: "18px",
        lineHeight: "24px",
      },
    },
    md: {
      control: {
        width: "20px",
        height: "20px",
      },
      label: {
        fontWeight: "500",
        fontSize: "16px",
        lineHeight: "20px",
      },
    },
    sm: {
      control: {
        width: "16px",
        height: "16px",
      },
      label: {
        fontWeight: "500",
        fontSize: "14px",
        lineHeight: "16px",
      },
    },
  },
  variants: {
    plain: (props: StyleFunctionProps) => ({
      control: {
        border: "1px solid",
        borderColor: "frethanAshGrey",
        backgroundColor: props.colorMode !== "dark" ? "frethanWhite" : "none",
        borderRadius: "6px",
      },
      label: {
        color: "frethanBlack",
      },
    }),
    solid: (props: StyleFunctionProps) => ({
      control: {
        border: "2px solid",
        borderColor: "frethanSoftBlue",
        backgroundColor: props.colorMode !== "dark" ? "frethanWhite" : "none",
        borderRadius: "6px",
      },
      label: {
        color: "frethanBlack",
      },
    }),
    glow: (props: StyleFunctionProps) => ({
      control: {
        border: "1px solid",
        borderColor: "frethanSoftBlue",
        backgroundColor: props.colorMode !== "dark" ? "frethanWhite" : "none",
        boxShadow: "0 0 3px 3px rgba(78, 138, 247, 0.4)",
        borderRadius: "6px",
      },
      label: {
        color: "frethanBlack",
      },
    }),
    danger: {
      control: {
        border: "1px solid",
        borderColor: "frethanCavernPink",
        backgroundColor: "frethanMistyRose",
        borderRadius: "6px",
      },
      label: {
        color: "frethanBlack",
      },
    },
    disabled: {
      control: {
        border: "1px solid",
        borderColor: "frethanIron",
        backgroundColor: "frethanIron",
        borderRadius: "6px",
      },
      label: {
        color: "frethanGrey",
      },
    },
  },
};

const RadioControlDark = {
  bg: "neutralBackground.200",
  borderColor: "secondaryBackground.200",
  _checked: {
    bg: "primaryIcon.500",
    border: "none",
    _hover: {
      bg: "primary.350",
    },
  },
  _hover: {
    borderColor: "primary.350",
  },
  _invalid: {
    bg: "frethanDarkRed",
    borderColor: "errorStroke.200",
    _checked: { bg: "#DD3C41", border: "none" },
  },
  _disabled: {
    bg: "#334073",
    _hover: {
      bg: "#334073",
      border: "none",
    },
    _invalid: {
      bg: "#334073",
      border: "none",
    },
  },
};

const Radio = {
  baseStyle: {
    label: {
      color: "#0D142F",
      _disabled: {
        color: "#8799DA",
        opacity: 1,
      },
      _dark: {
        color: "primaryTypography.200",
        _disabled: { color: "#6676B7" },
      },
    },
    control: {
      bg: "frethanWhite",
      borderColor: "secondaryStroke.500",
      _checked: {
        bg: "primaryIcon.200",
        border: "none",
        _hover: {
          bg: "secondary.500",
        },
      },
      _hover: {
        borderColor: "secondary.500",
      },
      _invalid: {
        bg: "frethanMistyRose",
        borderColor: "errorStroke.500",
        _checked: {
          bg: "errorIcon.500",
          border: "none",
          _hover: {
            bg: "errorIcon.500",
          },
        },
        _hover: {
          bg: "frethanMistyRose",
          borderColor: "errorStroke.500",
        },
      },
      _disabled: {
        bg: "secondaryStroke.500",
        borderColor: "secondaryStroke.500",
        _hover: {
          borderColor: "secondaryStroke.500",
        },
        _invalid: {
          bg: "secondaryStroke.500",
          borderColor: "secondaryStroke.500",
          _hover: {
            bg: "secondaryStroke.500",
            borderColor: "secondaryStroke.500",
          },
        },
        _checked: {
          bg: "secondaryStroke.500",
          color: "#8799DA",
          _invalid: {
            bg: "secondaryStroke.500",
            _hover: {
              bg: "secondaryStroke.500",
              borderColor: "secondaryStroke.500",
            },
          },
        },
      },
      _dark: RadioControlDark,
    },
  },
};

const Tag = {
  variants: {
    neutral: (props: StyleFunctionProps) => ({
      container:
        props.colorMode === "dark"
          ? {
              bg: "white",
              color: "primaryTypography.500",
            }
          : {
              bg: "secondaryBackground.500",
              color: "primaryTypography.500",
            },
    }),
    success: (props: StyleFunctionProps) => ({
      container:
        props.colorMode === "dark"
          ? {
              bg: "#2D8B70",
              color: "white",
            }
          : {
              bg: "success.150",
              color: "success.500",
            },
    }),
    error: (props: StyleFunctionProps) => ({
      container:
        props.colorMode === "dark"
          ? {
              bg: "#DD3C41",
              color: "white",
            }
          : {
              bg: "frethanMistyRose",
              color: "#CA151A",
            },
    }),
    warning: (props: StyleFunctionProps) => ({
      container:
        props.colorMode === "dark"
          ? {
              bg: "#D3742F",
              color: "white",
            }
          : {
              bg: "frethanOrange",
              color: "warningIcon.500",
            },
    }),
    alert: (props: StyleFunctionProps) => ({
      container:
        props.colorMode === "dark"
          ? {
              bg: "#F2BC38",
              color: "white",
            }
          : {
              bg: "alert.150",
              color: "primaryTypography.500",
            },
    }),
    information: (props: StyleFunctionProps) => ({
      container:
        props.colorMode === "dark"
          ? {
              bg: "#2580A7",
              color: "white",
            }
          : {
              bg: "information.150",
              color: "infoIcon.500",
            },
    }),
  },
};

const HStack = {
  variant: {
    plain: {
      border: "1px solid",
      borderColor: "frethanAshGrey",
      backgroundColor: "frethanWhite",
      borderRadius: "6px",
      padding: "12px 0 12px 12px",
    },
  },
};

const baseStyle = definePartsStyle({
  track: {
    bg: "tertiaryBackground.500",
    _checked: {
      bg: "primaryGlow",
      _disabled: {
        bg: "secondaryStroke.500",
      },
    },
    _disabled: {
      bg: "secondaryStroke.500",
    },
    _dark: {
      bg: "tertiaryBackground.200",
      _checked: { bg: "primaryIcon.500", _disabled: "#334073" },
      _disabled: { bg: "#334073" },
    },
  },
});

const Switch = defineMultiStyleConfig({
  baseStyle,
  variants: {
    plain: {
      thumb: {
        bg: "frethanWhite",
        _dark: {
          bg: "neutralBackground.200",
        },
      },
    },
    disabled: {
      thumb: {
        bg: "#8799DA",
        _dark: {
          bg: "#6676B7",
        },
      },
    },
  },
});

const components = {
  Input,
  Text,
  Heading,
  Button,
  Link,
  Checkbox,
  HStack,
  Switch,
  Tag,
  Radio,
  Select,
  Textarea
};

export default components;
