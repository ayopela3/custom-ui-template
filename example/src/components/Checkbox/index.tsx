import { FC } from 'react'
import { Checkbox, CheckboxProps } from '@chakra-ui/react'
import { CheckIconBox } from '../../icons'
import { CheckboxSizes, CheckboxVariants } from '../../lib/types'
import { getCheckboxIconColor, getCheckboxIconSize } from '../../lib/utils'
import colors from '../../components/themes/colors'

interface CustomIcon {
  isChecked: boolean
  iconSize: string
  checkBgColor: string
  checkColor: string
}

interface CheckboxesProps {
  label?: string
  size?: CheckboxSizes
  variant?: CheckboxVariants
  checkboxProps?: CheckboxProps
}

const CustomIcon: FC<CustomIcon> = ({
  isChecked,
  iconSize,
  checkBgColor,
  checkColor,
}) => {
  return isChecked ? (
    <CheckIconBox
      h={iconSize}
      w={iconSize}
      checkBgColor={checkBgColor}
      checkColor={checkColor}
    />
  ) : null
}

export const CheckboxComponent: FC<CheckboxesProps> = ({
  label,
  size,
  variant,
  checkboxProps,
}) => {
  const currentCheckboxSize: CheckboxSizes = size || 'md'
  const currentVariant: CheckboxVariants = variant || 'plain'
  const checkColor =
    currentVariant === 'disabled' ? colors.frethanGrey : colors.frethanWhite
  const iconSize = getCheckboxIconSize(currentCheckboxSize)
  const checkboxIconColor = getCheckboxIconColor(currentVariant)

  return (
    <Checkbox
      icon={
        <CustomIcon
          isChecked={false}
          iconSize={iconSize}
          checkBgColor={checkboxIconColor}
          checkColor={checkColor}
        />
      }
      size={currentCheckboxSize}
      variant={currentVariant}
      {...checkboxProps}
    >
      {label}
    </Checkbox>
  )
}
