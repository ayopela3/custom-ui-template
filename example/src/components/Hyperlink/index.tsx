import { ArrowLeftIcon, ArrowRightIcon } from '../../icons'
import { hypertextColors, hypertextSizes } from '../../constants'
import {
  HyperlinkTextSize,
  HyperlinkTextVariants,
  HyperlinkVariants,
} from '../../lib/types'
import { Link, HStack } from '@chakra-ui/react'
import { FC } from 'react'

interface HyperlinkProps {
  text: string
  hasLeftArrow: boolean
  hasRightArow: boolean
  size: HyperlinkTextSize
  variants?: HyperlinkTextVariants
}

export const Hyperlink: FC<HyperlinkProps> = ({
  text,
  hasLeftArrow = false,
  hasRightArow = false,
  size,
  variants,
}) => {
  const currentVariant = variants || HyperlinkVariants.HYPERLINK_NORMAL
  const isVariantGlow =
    currentVariant && currentVariant === HyperlinkVariants.HYPERLINK_GLOW
  const hypertextColor = hypertextColors[currentVariant]
  const hyperlinSize = hypertextSizes[size]

  return (
    <HStack
      spacing={2}
      backgroundColor={isVariantGlow ? 'frethanWhite' : 'transparent'}
      border={isVariantGlow ? '5px solid rgba(78, 138, 247, 0.3)' : 'none'}
      borderRadius="8px"
      alignItems="center"
      w="fit-content"
      p={isVariantGlow ? '2px 5px' : '0'}
    >
      {hasLeftArrow && (
        <ArrowLeftIcon
          color={hypertextColor}
          h={hyperlinSize.arrowProp.height}
          w={hyperlinSize.arrowProp.width}
        />
      )}
      <Link
        variant={currentVariant}
        fontSize={hyperlinSize.textProp.fontSize}
        lineHeight={hyperlinSize.textProp.lineHeight}
        color={hypertextColor}
      >
        {text}
      </Link>
      {hasRightArow && (
        <ArrowRightIcon
          color={hypertextColor}
          h={hyperlinSize.arrowProp.height}
          w={hyperlinSize.arrowProp.width}
        />
      )}
    </HStack>
  )
}
