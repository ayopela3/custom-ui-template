import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import { CheckboxComponent } from '../components/Checkbox'

export default {
  title: 'Components/Checkbox',
  component: CheckboxComponent,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as ComponentMeta<typeof CheckboxComponent>

const Template: ComponentStory<typeof CheckboxComponent> = (args) => (
  <CheckboxComponent {...args} />
)

export const Regular = Template.bind({})

Regular.args = {
  label: 'Regular',
  size: 'md',
  variant: 'plain',
}
