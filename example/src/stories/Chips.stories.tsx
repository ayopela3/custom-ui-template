import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import { Chips } from '../components/Chips'

export default {
  title: 'Components/Chips',
  component: Chips,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as ComponentMeta<typeof Chips>

const Template: ComponentStory<typeof Chips> = (args) => <Chips {...args} />

export const Regular = Template.bind({})

Regular.args = {
  label: 'Regular',
  size: 'md',
  avatar: 'https://bit.ly/dan-abramov',
  isClosable: true,
}
