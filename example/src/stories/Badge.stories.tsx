import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'
import { VARIANTS } from '../constants/options'

import { Badge } from '../components/Badge'

export default {
  title: 'Components/Badge',
  component: Badge,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as ComponentMeta<typeof Badge>

const Template: ComponentStory<typeof Badge> = (args) => <Badge {...args} />

export const Primary = Template.bind({})

Primary.args = {
  fontWeight: 600,
  label: 'Badge',
  backgroundColor: '#EFEFF1',
  variant: VARIANTS.solid,
}

export const Secondary = Template.bind({})
Secondary.args = {
  fontWeight: 600,
  label: 'Badge',
  textColor: '#fff',
  backgroundColor: '#CE6418',
  variant: VARIANTS.solid,
}
