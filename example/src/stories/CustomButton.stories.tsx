import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'
import { ArrowBackIcon, ArrowForwardIcon } from '@chakra-ui/icons'

import { CustomButton } from '../components/Buttons'

export default {
  title: 'Components/Button',
  component: CustomButton,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as ComponentMeta<typeof CustomButton>

const Template: ComponentStory<typeof CustomButton> = (args) => (
  <CustomButton {...args} />
)

export const Primary = Template.bind({})

Primary.args = {
  content: 'Button',
  leftIcon: <ArrowBackIcon />,
  rightIcon: <ArrowForwardIcon />,
  variant: 'solid',
}

export const PrimaryOutlined = Template.bind({})

PrimaryOutlined.args = {
  content: 'Button',
  leftIcon: <ArrowBackIcon />,
  rightIcon: <ArrowForwardIcon />,
  variant: 'outline',
}
