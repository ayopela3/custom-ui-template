import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import { CalendarView } from '../components/Calendar'

export default {
  title: 'Components/CalendarView',
  component: CalendarView,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as ComponentMeta<typeof CalendarView>

const Template: ComponentStory<typeof CalendarView> = (args) => (
  <CalendarView {...args} />
)

export const LightCalendar = Template.bind({})

export const DarkCalendar = Template.bind({})
DarkCalendar.args = {
  numberOfMonths: 2,
  colorMode: 'dark',
}
